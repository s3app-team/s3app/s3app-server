CREATE TABLE IF NOT EXISTS public."Thought"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "authorUserId" uuid,
    description text COLLATE pg_catalog."default",
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "domainId" uuid NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now(),
    CONSTRAINT thought_pkey PRIMARY KEY (id),
    CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId")
        REFERENCES public."User" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_domainid FOREIGN KEY ("domainId")
        REFERENCES public."Domain" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

ALTER TABLE IF EXISTS public."Comment"
    ADD COLUMN "thoughtId" uuid;
ALTER TABLE IF EXISTS public."Comment"
    ADD FOREIGN KEY ("thoughtId")
    REFERENCES public."Thought" (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;
