alter table "User"
add column    license character varying COLLATE pg_catalog."default",
add column    "licenseExpires" timestamp without time zone;
