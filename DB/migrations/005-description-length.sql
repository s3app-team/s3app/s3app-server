ALTER TABLE public."Issue"
    ALTER COLUMN description TYPE text COLLATE pg_catalog."default";

ALTER TABLE public."Comment"
    ALTER COLUMN description TYPE text COLLATE pg_catalog."default";

ALTER TABLE public."Domain"
    ALTER COLUMN description TYPE text COLLATE pg_catalog."default";

ALTER TABLE public."Driver"
    ALTER COLUMN description TYPE text COLLATE pg_catalog."default";

ALTER TABLE public."Proposal"
    ALTER COLUMN description TYPE text COLLATE pg_catalog."default";

ALTER TABLE public."Sprint"
    ALTER COLUMN goal TYPE text COLLATE pg_catalog."default";

ALTER TABLE public."Tension"
    ALTER COLUMN description TYPE text COLLATE pg_catalog."default";

