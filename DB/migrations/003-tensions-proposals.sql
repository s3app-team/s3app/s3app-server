CREATE TYPE public.tension_mark AS ENUM
    ('+', '-', '0');

CREATE TABLE IF NOT EXISTS public."Driver"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "authorUserId" uuid,
    "tensionType" tension_type,
    "isPositive" boolean,
    description character varying(255) COLLATE pg_catalog."default",
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" time without time zone DEFAULT now(),
    "domainId" uuid NOT NULL,
    CONSTRAINT driver_pkey PRIMARY KEY (id),
    CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId")
        REFERENCES public."User" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_domainid FOREIGN KEY ("domainId")
        REFERENCES public."Domain" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

CREATE TABLE IF NOT EXISTS public."TensionDriver"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "tensionId" uuid NOT NULL,
    "driverId" uuid NOT NULL,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    CONSTRAINT "TensionDriver_pkey" PRIMARY KEY ("tensionId", "driverId"),
    CONSTRAINT fk_driverid FOREIGN KEY ("driverId")
        REFERENCES public."Driver" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_tensionid FOREIGN KEY ("tensionId")
        REFERENCES public."Tension" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS public."DriverProposal"
(
    id uuid NOT NULL DEFAULT gen_random_uuid(),
    "driverId" uuid NOT NULL,
    "proposalId" uuid NOT NULL,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    CONSTRAINT "DriverProposal_pkey" PRIMARY KEY ("driverId", "proposalId"),
    CONSTRAINT fk_proposalid FOREIGN KEY ("proposalId")
        REFERENCES public."Proposal" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_driverid FOREIGN KEY ("driverId")
        REFERENCES public."Driver" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

DROP TABLE "TensionProposal";

ALTER TABLE IF EXISTS public."Tension" DROP COLUMN IF EXISTS "tensionType";

ALTER TABLE IF EXISTS public."Tension" DROP COLUMN IF EXISTS "isPositive";

ALTER TABLE IF EXISTS public."Tension"
    ADD COLUMN "tensionMark" tension_mark NOT NULL;

ALTER TABLE IF EXISTS public."Tension"
    ADD COLUMN name character varying NOT NULL;

ALTER TABLE IF EXISTS public."Driver"
    ADD COLUMN name character varying NOT NULL;

ALTER TABLE IF EXISTS public."Proposal"
    ADD COLUMN name character varying NOT NULL;

ALTER TABLE IF EXISTS public."Tension" DROP COLUMN IF EXISTS "createdAt";

ALTER TABLE IF EXISTS public."Tension"
    ADD COLUMN "createdAt" timestamp without time zone DEFAULT now();

ALTER TABLE IF EXISTS public."Driver" DROP COLUMN IF EXISTS "createdAt";

ALTER TABLE IF EXISTS public."Driver"
    ADD COLUMN "createdAt" timestamp without time zone DEFAULT now();

ALTER TABLE IF EXISTS public."TensionDriver" DROP COLUMN IF EXISTS "isDeleted";

ALTER TABLE IF EXISTS public."TensionDriver" DROP COLUMN IF EXISTS "deletedAt";

ALTER TABLE IF EXISTS public."DriverProposal" DROP COLUMN IF EXISTS "isDeleted";

ALTER TABLE IF EXISTS public."DriverProposal" DROP COLUMN IF EXISTS "deletedAt";

ALTER TABLE IF EXISTS public."Domain" DROP CONSTRAINT IF EXISTS fk_maindriverid;

ALTER TABLE IF EXISTS public."Domain"
    ADD CONSTRAINT fk_maindriverid FOREIGN KEY ("mainDriverId")
    REFERENCES public."Driver" (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID;
