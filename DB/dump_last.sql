--
-- PostgreSQL database dump
--

-- Dumped from database version 16.1 (Debian 16.1-1.pgdg120+1)
-- Dumped by pg_dump version 16.0

-- Started on 2024-06-24 11:34:23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 864 (class 1247 OID 17679)
-- Name: authorization_role; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.authorization_role AS ENUM (
    'user',
    'admin'
);


--
-- TOC entry 867 (class 1247 OID 17684)
-- Name: domain_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.domain_type AS ENUM (
    'service',
    'circle',
    'role',
    'organization'
);


--
-- TOC entry 960 (class 1247 OID 19519)
-- Name: driver_effect; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.driver_effect AS ENUM (
    '+',
    '-',
    '0'
);


--
-- TOC entry 870 (class 1247 OID 17694)
-- Name: event_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.event_type AS ENUM (
    'planning',
    'review',
    'retrospective',
    'management',
    'choiceForRole'
);


--
-- TOC entry 873 (class 1247 OID 17706)
-- Name: history_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.history_type AS ENUM (
    'create',
    'update',
    'newExecutor',
    'executorStartedWork',
    'newSprints'
);


--
-- TOC entry 876 (class 1247 OID 17718)
-- Name: link_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.link_type AS ENUM (
    'consist',
    'delegate'
);


--
-- TOC entry 939 (class 1247 OID 19331)
-- Name: organization_access_role_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.organization_access_role_type AS ENUM (
    'user',
    'admin'
);


--
-- TOC entry 879 (class 1247 OID 17724)
-- Name: proposal_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.proposal_status AS ENUM (
    'new',
    'accepted',
    'holded',
    'rejected'
);


--
-- TOC entry 882 (class 1247 OID 17734)
-- Name: situation_description_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.situation_description_status AS ENUM (
    'accepted',
    'notAccepted',
    'consideration'
);


--
-- TOC entry 948 (class 1247 OID 19442)
-- Name: tension_mark; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.tension_mark AS ENUM (
    '+',
    '-',
    '0'
);


--
-- TOC entry 885 (class 1247 OID 17742)
-- Name: tension_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.tension_type AS ENUM (
    'tension',
    'driver'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 215 (class 1259 OID 17747)
-- Name: Comment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Comment" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    description text,
    "authorUserId" uuid,
    "issueId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "tensionId" uuid,
    "driverId" uuid,
    "proposalId" uuid,
    "thoughtId" uuid
);


--
-- TOC entry 216 (class 1259 OID 17753)
-- Name: Domain; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Domain" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    "domainType" public.domain_type,
    "isOpen" boolean,
    "mainDriverId" uuid,
    "parentDomainId" uuid,
    "implementationStrategy" character varying(255),
    restrictions character varying(255),
    "performanceCriteria" character varying(255),
    description text,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 234 (class 1259 OID 19449)
-- Name: Driver; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Driver" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "authorUserId" uuid,
    "tensionType" public.tension_type,
    "isPositive" boolean,
    description text,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "domainId" uuid NOT NULL,
    name character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now(),
    "driverEffect" public.driver_effect
);


--
-- TOC entry 237 (class 1259 OID 19533)
-- Name: DriverIssue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."DriverIssue" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "driverId" uuid NOT NULL,
    "issueId" uuid NOT NULL,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 236 (class 1259 OID 19485)
-- Name: DriverProposal; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."DriverProposal" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "driverId" uuid NOT NULL,
    "proposalId" uuid NOT NULL,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 217 (class 1259 OID 17761)
-- Name: Event; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Event" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    "domainId" uuid,
    type public.event_type,
    "startDate" timestamp without time zone,
    "endDate" timestamp without time zone,
    "facilitatorUserId" uuid,
    "planningEventId" uuid,
    "reviewEventId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 218 (class 1259 OID 17767)
-- Name: EventParticipant; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."EventParticipant" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "eventId" uuid,
    "userId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 219 (class 1259 OID 17773)
-- Name: History; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."History" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    description character varying(255),
    date timestamp without time zone,
    type public.history_type,
    "issueId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 220 (class 1259 OID 17781)
-- Name: Issue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Issue" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "domainId" uuid,
    "authorUserId" uuid,
    name character varying(255),
    description text,
    "tensionId" uuid,
    "executorUserId" uuid,
    "sprintId" uuid,
    "issueTypeId" uuid,
    "stageId" uuid,
    version integer,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    priority integer,
    "isClosed" boolean DEFAULT false NOT NULL,
    "closedAt" timestamp without time zone,
    "deadlineAt" timestamp without time zone
);


--
-- TOC entry 221 (class 1259 OID 17789)
-- Name: IssueLink; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."IssueLink" (
    "fromIssueId" uuid NOT NULL,
    "toIssueId" uuid NOT NULL,
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "deletedAt" time without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "isDeleted" boolean,
    "updatedAt" timestamp without time zone
);


--
-- TOC entry 222 (class 1259 OID 17794)
-- Name: IssueType; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."IssueType" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    description character varying(255),
    "domainId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 239 (class 1259 OID 21361)
-- Name: IssueTypeStage; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."IssueTypeStage" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "issueTypeId" uuid NOT NULL,
    "stageId" uuid NOT NULL,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 223 (class 1259 OID 17802)
-- Name: Proposal; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Proposal" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "authorUserId" uuid,
    status public.proposal_status,
    description text,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "domainId" uuid NOT NULL,
    name character varying NOT NULL,
    "previousProposalId" uuid
);


--
-- TOC entry 224 (class 1259 OID 17808)
-- Name: SituationDescription; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."SituationDescription" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    description character varying(255),
    status public.situation_description_status,
    "domainId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "mongoId" uuid
);


--
-- TOC entry 225 (class 1259 OID 17814)
-- Name: Sprint; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Sprint" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    goal text,
    "startDate" timestamp without time zone,
    "endDate" timestamp without time zone,
    "planningEventId" uuid,
    "reviewEventId" uuid,
    "domainId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 226 (class 1259 OID 17822)
-- Name: Stage; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Stage" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    "domainId" uuid,
    "IssueTypeId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "order" integer DEFAULT 0 NOT NULL,
    color character varying,
    "isStart" boolean DEFAULT false,
    "isEnd" boolean DEFAULT false
);


--
-- TOC entry 227 (class 1259 OID 17828)
-- Name: StageLink; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."StageLink" (
    "previousStageId" uuid NOT NULL,
    "nextStageId" uuid NOT NULL,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 228 (class 1259 OID 17833)
-- Name: Tension; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Tension" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "authorUserId" uuid,
    description text,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "domainId" uuid NOT NULL,
    "tensionMark" public.tension_mark,
    name character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 235 (class 1259 OID 19467)
-- Name: TensionDriver; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."TensionDriver" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "tensionId" uuid NOT NULL,
    "driverId" uuid NOT NULL,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 238 (class 1259 OID 21311)
-- Name: Thought; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Thought" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "authorUserId" uuid,
    description text,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "domainId" uuid NOT NULL,
    name character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 229 (class 1259 OID 17845)
-- Name: User; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."User" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    email character varying(255),
    password character varying(255),
    "domainId" uuid,
    "resetPasswordExpires" timestamp without time zone,
    "resetPasswordCode" character varying(255),
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "confirmEmailCode" character varying(255),
    "confirmEmail" character varying(255),
    "confirmEmailExpires" timestamp without time zone,
    license character varying,
    "licenseExpires" timestamp without time zone
);


--
-- TOC entry 230 (class 1259 OID 17853)
-- Name: UserDomainLink; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."UserDomainLink" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "userId" uuid,
    "domainId" uuid,
    "delegateDomainId" uuid,
    "linkType" public.link_type,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 233 (class 1259 OID 19351)
-- Name: UserOrganizationInvite; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."UserOrganizationInvite" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    email character varying(255) NOT NULL,
    name character varying(255),
    license character varying(255),
    "organizationId" uuid NOT NULL,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "inviterUserId" uuid NOT NULL,
    "invitedUserId" uuid,
    "isAccepted" boolean DEFAULT false NOT NULL,
    "acceptedAt" timestamp without time zone
);


--
-- TOC entry 232 (class 1259 OID 19335)
-- Name: UserOrganizationLink; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."UserOrganizationLink" (
    "userId" uuid NOT NULL,
    "organizationId" uuid NOT NULL,
    "accessRole" public.organization_access_role_type NOT NULL,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 231 (class 1259 OID 17859)
-- Name: UserSprint; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."UserSprint" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "userId" uuid,
    "sprintId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


--
-- TOC entry 3405 (class 2606 OID 17866)
-- Name: Comment Comment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Comment"
    ADD CONSTRAINT "Comment_pkey" PRIMARY KEY (id);


--
-- TOC entry 3407 (class 2606 OID 17868)
-- Name: Domain Domain_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Domain"
    ADD CONSTRAINT "Domain_pkey" PRIMARY KEY (id);


--
-- TOC entry 3449 (class 2606 OID 19539)
-- Name: DriverIssue DriverIssue_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."DriverIssue"
    ADD CONSTRAINT "DriverIssue_pkey" PRIMARY KEY ("driverId", "issueId");


--
-- TOC entry 3447 (class 2606 OID 19492)
-- Name: DriverProposal DriverProposal_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."DriverProposal"
    ADD CONSTRAINT "DriverProposal_pkey" PRIMARY KEY ("driverId", "proposalId");


--
-- TOC entry 3411 (class 2606 OID 17870)
-- Name: EventParticipant EventParticipant_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."EventParticipant"
    ADD CONSTRAINT "EventParticipant_pkey" PRIMARY KEY (id);


--
-- TOC entry 3409 (class 2606 OID 17872)
-- Name: Event Event_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT "Event_pkey" PRIMARY KEY (id);


--
-- TOC entry 3413 (class 2606 OID 17874)
-- Name: History History_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."History"
    ADD CONSTRAINT "History_pkey" PRIMARY KEY (id);


--
-- TOC entry 3417 (class 2606 OID 17876)
-- Name: IssueLink IssueLink_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."IssueLink"
    ADD CONSTRAINT "IssueLink_pkey" PRIMARY KEY (id);


--
-- TOC entry 3453 (class 2606 OID 21367)
-- Name: IssueTypeStage IssueTypeStage_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."IssueTypeStage"
    ADD CONSTRAINT "IssueTypeStage_pkey" PRIMARY KEY ("issueTypeId", "stageId");


--
-- TOC entry 3419 (class 2606 OID 17878)
-- Name: IssueType IssueType_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."IssueType"
    ADD CONSTRAINT "IssueType_pkey" PRIMARY KEY (id);


--
-- TOC entry 3415 (class 2606 OID 17880)
-- Name: Issue Issue_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT "Issue_pkey" PRIMARY KEY (id);


--
-- TOC entry 3421 (class 2606 OID 17882)
-- Name: Proposal Proposal_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Proposal"
    ADD CONSTRAINT "Proposal_pkey" PRIMARY KEY (id);


--
-- TOC entry 3423 (class 2606 OID 17884)
-- Name: SituationDescription SituationDescription_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."SituationDescription"
    ADD CONSTRAINT "SituationDescription_pkey" PRIMARY KEY (id);


--
-- TOC entry 3425 (class 2606 OID 17886)
-- Name: Sprint Sprint_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT "Sprint_pkey" PRIMARY KEY (id);


--
-- TOC entry 3429 (class 2606 OID 17888)
-- Name: StageLink StageLink_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."StageLink"
    ADD CONSTRAINT "StageLink_pkey" PRIMARY KEY ("previousStageId", "nextStageId");


--
-- TOC entry 3427 (class 2606 OID 17890)
-- Name: Stage Stage_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Stage"
    ADD CONSTRAINT "Stage_pkey" PRIMARY KEY (id);


--
-- TOC entry 3445 (class 2606 OID 19474)
-- Name: TensionDriver TensionDriver_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."TensionDriver"
    ADD CONSTRAINT "TensionDriver_pkey" PRIMARY KEY ("tensionId", "driverId");


--
-- TOC entry 3435 (class 2606 OID 17894)
-- Name: UserDomainLink UserDomainLink_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserDomainLink"
    ADD CONSTRAINT "UserDomainLink_pkey" PRIMARY KEY (id);


--
-- TOC entry 3441 (class 2606 OID 19361)
-- Name: UserOrganizationInvite UserOrganizationInvite_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserOrganizationInvite"
    ADD CONSTRAINT "UserOrganizationInvite_pkey" PRIMARY KEY (id);


--
-- TOC entry 3439 (class 2606 OID 19340)
-- Name: UserOrganizationLink UserOrganizationLink_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserOrganizationLink"
    ADD CONSTRAINT "UserOrganizationLink_pkey" PRIMARY KEY ("userId", "organizationId");


--
-- TOC entry 3437 (class 2606 OID 17896)
-- Name: UserSprint UserSprint_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserSprint"
    ADD CONSTRAINT "UserSprint_pkey" PRIMARY KEY (id);


--
-- TOC entry 3433 (class 2606 OID 17898)
-- Name: User User_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (id);


--
-- TOC entry 3443 (class 2606 OID 19456)
-- Name: Driver driver_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Driver"
    ADD CONSTRAINT driver_pkey PRIMARY KEY (id);


--
-- TOC entry 3431 (class 2606 OID 17900)
-- Name: Tension tension_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Tension"
    ADD CONSTRAINT tension_pkey PRIMARY KEY (id);


--
-- TOC entry 3451 (class 2606 OID 21320)
-- Name: Thought thought_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Thought"
    ADD CONSTRAINT thought_pkey PRIMARY KEY (id);


--
-- TOC entry 3454 (class 2606 OID 21331)
-- Name: Comment Comment_thoughtId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Comment"
    ADD CONSTRAINT "Comment_thoughtId_fkey" FOREIGN KEY ("thoughtId") REFERENCES public."Thought"(id) NOT VALID;


--
-- TOC entry 3476 (class 2606 OID 19525)
-- Name: Proposal Proposal_previousProposalId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Proposal"
    ADD CONSTRAINT "Proposal_previousProposalId_fkey" FOREIGN KEY ("previousProposalId") REFERENCES public."Proposal"(id) NOT VALID;


--
-- TOC entry 3497 (class 2606 OID 19362)
-- Name: UserOrganizationInvite UserOrganizationInvite_invitedUserId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserOrganizationInvite"
    ADD CONSTRAINT "UserOrganizationInvite_invitedUserId_fkey" FOREIGN KEY ("invitedUserId") REFERENCES public."User"(id);


--
-- TOC entry 3498 (class 2606 OID 19367)
-- Name: UserOrganizationInvite UserOrganizationInvite_inviterId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserOrganizationInvite"
    ADD CONSTRAINT "UserOrganizationInvite_inviterId_fkey" FOREIGN KEY ("inviterUserId") REFERENCES public."User"(id);


--
-- TOC entry 3495 (class 2606 OID 19341)
-- Name: UserOrganizationLink UserOrganizationLink_organizationid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserOrganizationLink"
    ADD CONSTRAINT "UserOrganizationLink_organizationid_fkey" FOREIGN KEY ("organizationId") REFERENCES public."Domain"(id);


--
-- TOC entry 3496 (class 2606 OID 19346)
-- Name: UserOrganizationLink UserOrganizationLink_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserOrganizationLink"
    ADD CONSTRAINT "UserOrganizationLink_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."User"(id);


--
-- TOC entry 3487 (class 2606 OID 17901)
-- Name: Tension fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Tension"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 3477 (class 2606 OID 17906)
-- Name: Proposal fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Proposal"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 3455 (class 2606 OID 17911)
-- Name: Comment fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Comment"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 3466 (class 2606 OID 17916)
-- Name: Issue fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 3500 (class 2606 OID 19457)
-- Name: Driver fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Driver"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 3508 (class 2606 OID 21321)
-- Name: Thought fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Thought"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 3490 (class 2606 OID 17921)
-- Name: UserDomainLink fk_delegatedomainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserDomainLink"
    ADD CONSTRAINT fk_delegatedomainid FOREIGN KEY ("delegateDomainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3491 (class 2606 OID 17926)
-- Name: UserDomainLink fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserDomainLink"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3489 (class 2606 OID 17931)
-- Name: User fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3475 (class 2606 OID 17936)
-- Name: IssueType fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."IssueType"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3483 (class 2606 OID 17941)
-- Name: Stage fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Stage"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3459 (class 2606 OID 17946)
-- Name: Event fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3479 (class 2606 OID 17951)
-- Name: SituationDescription fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."SituationDescription"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3480 (class 2606 OID 17956)
-- Name: Sprint fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3467 (class 2606 OID 17961)
-- Name: Issue fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3478 (class 2606 OID 19377)
-- Name: Proposal fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Proposal"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id) NOT VALID;


--
-- TOC entry 3488 (class 2606 OID 19382)
-- Name: Tension fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Tension"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id) NOT VALID;


--
-- TOC entry 3501 (class 2606 OID 19462)
-- Name: Driver fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Driver"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3509 (class 2606 OID 21326)
-- Name: Thought fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Thought"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3502 (class 2606 OID 19475)
-- Name: TensionDriver fk_driverid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."TensionDriver"
    ADD CONSTRAINT fk_driverid FOREIGN KEY ("driverId") REFERENCES public."Driver"(id);


--
-- TOC entry 3504 (class 2606 OID 19498)
-- Name: DriverProposal fk_driverid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."DriverProposal"
    ADD CONSTRAINT fk_driverid FOREIGN KEY ("driverId") REFERENCES public."Driver"(id);


--
-- TOC entry 3506 (class 2606 OID 19540)
-- Name: DriverIssue fk_driverid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."DriverIssue"
    ADD CONSTRAINT fk_driverid FOREIGN KEY ("driverId") REFERENCES public."Driver"(id);


--
-- TOC entry 3463 (class 2606 OID 17966)
-- Name: EventParticipant fk_eventid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."EventParticipant"
    ADD CONSTRAINT fk_eventid FOREIGN KEY ("eventId") REFERENCES public."Event"(id);


--
-- TOC entry 3468 (class 2606 OID 17971)
-- Name: Issue fk_executoruserid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_executoruserid FOREIGN KEY ("executorUserId") REFERENCES public."User"(id);


--
-- TOC entry 3460 (class 2606 OID 17976)
-- Name: Event fk_facilitatoruserid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT fk_facilitatoruserid FOREIGN KEY ("facilitatorUserId") REFERENCES public."User"(id);


--
-- TOC entry 3465 (class 2606 OID 17981)
-- Name: History fk_issueid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."History"
    ADD CONSTRAINT fk_issueid FOREIGN KEY ("issueId") REFERENCES public."Issue"(id);


--
-- TOC entry 3456 (class 2606 OID 17986)
-- Name: Comment fk_issueid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Comment"
    ADD CONSTRAINT fk_issueid FOREIGN KEY ("issueId") REFERENCES public."Issue"(id);


--
-- TOC entry 3507 (class 2606 OID 19545)
-- Name: DriverIssue fk_issueid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."DriverIssue"
    ADD CONSTRAINT fk_issueid FOREIGN KEY ("issueId") REFERENCES public."Issue"(id);


--
-- TOC entry 3484 (class 2606 OID 17991)
-- Name: Stage fk_issuetypeid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Stage"
    ADD CONSTRAINT fk_issuetypeid FOREIGN KEY ("IssueTypeId") REFERENCES public."IssueType"(id);


--
-- TOC entry 3469 (class 2606 OID 17996)
-- Name: Issue fk_issuetypeid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_issuetypeid FOREIGN KEY ("issueTypeId") REFERENCES public."IssueType"(id);


--
-- TOC entry 3510 (class 2606 OID 21368)
-- Name: IssueTypeStage fk_issuetypeid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."IssueTypeStage"
    ADD CONSTRAINT fk_issuetypeid FOREIGN KEY ("issueTypeId") REFERENCES public."IssueType"(id);


--
-- TOC entry 3457 (class 2606 OID 19511)
-- Name: Domain fk_maindriverid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Domain"
    ADD CONSTRAINT fk_maindriverid FOREIGN KEY ("mainDriverId") REFERENCES public."Driver"(id) NOT VALID;


--
-- TOC entry 3485 (class 2606 OID 18006)
-- Name: StageLink fk_nextstageid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."StageLink"
    ADD CONSTRAINT fk_nextstageid FOREIGN KEY ("nextStageId") REFERENCES public."Stage"(id);


--
-- TOC entry 3499 (class 2606 OID 19372)
-- Name: UserOrganizationInvite fk_organizationid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserOrganizationInvite"
    ADD CONSTRAINT fk_organizationid FOREIGN KEY ("organizationId") REFERENCES public."Domain"(id);


--
-- TOC entry 3458 (class 2606 OID 18011)
-- Name: Domain fk_parentdomainid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Domain"
    ADD CONSTRAINT fk_parentdomainid FOREIGN KEY ("parentDomainId") REFERENCES public."Domain"(id);


--
-- TOC entry 3461 (class 2606 OID 18016)
-- Name: Event fk_planningeventid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT fk_planningeventid FOREIGN KEY ("planningEventId") REFERENCES public."Event"(id);


--
-- TOC entry 3481 (class 2606 OID 18021)
-- Name: Sprint fk_planningeventid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT fk_planningeventid FOREIGN KEY ("planningEventId") REFERENCES public."Event"(id);


--
-- TOC entry 3486 (class 2606 OID 18026)
-- Name: StageLink fk_previousstageid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."StageLink"
    ADD CONSTRAINT fk_previousstageid FOREIGN KEY ("previousStageId") REFERENCES public."Stage"(id);


--
-- TOC entry 3505 (class 2606 OID 19493)
-- Name: DriverProposal fk_proposalid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."DriverProposal"
    ADD CONSTRAINT fk_proposalid FOREIGN KEY ("proposalId") REFERENCES public."Proposal"(id);


--
-- TOC entry 3462 (class 2606 OID 18036)
-- Name: Event fk_revieweventid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT fk_revieweventid FOREIGN KEY ("reviewEventId") REFERENCES public."Event"(id);


--
-- TOC entry 3482 (class 2606 OID 18041)
-- Name: Sprint fk_revieweventid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT fk_revieweventid FOREIGN KEY ("reviewEventId") REFERENCES public."Event"(id);


--
-- TOC entry 3493 (class 2606 OID 18046)
-- Name: UserSprint fk_sprintid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserSprint"
    ADD CONSTRAINT fk_sprintid FOREIGN KEY ("sprintId") REFERENCES public."Sprint"(id);


--
-- TOC entry 3470 (class 2606 OID 18051)
-- Name: Issue fk_sprintid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_sprintid FOREIGN KEY ("sprintId") REFERENCES public."Sprint"(id);


--
-- TOC entry 3471 (class 2606 OID 18056)
-- Name: Issue fk_stageid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_stageid FOREIGN KEY ("stageId") REFERENCES public."Stage"(id);


--
-- TOC entry 3511 (class 2606 OID 21373)
-- Name: IssueTypeStage fk_stageid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."IssueTypeStage"
    ADD CONSTRAINT fk_stageid FOREIGN KEY ("stageId") REFERENCES public."Stage"(id);


--
-- TOC entry 3472 (class 2606 OID 18066)
-- Name: Issue fk_tensionid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_tensionid FOREIGN KEY ("tensionId") REFERENCES public."Tension"(id);


--
-- TOC entry 3503 (class 2606 OID 19480)
-- Name: TensionDriver fk_tensionid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."TensionDriver"
    ADD CONSTRAINT fk_tensionid FOREIGN KEY ("tensionId") REFERENCES public."Tension"(id);


--
-- TOC entry 3492 (class 2606 OID 18071)
-- Name: UserDomainLink fk_userid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserDomainLink"
    ADD CONSTRAINT fk_userid FOREIGN KEY ("userId") REFERENCES public."User"(id);


--
-- TOC entry 3464 (class 2606 OID 18076)
-- Name: EventParticipant fk_userid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."EventParticipant"
    ADD CONSTRAINT fk_userid FOREIGN KEY ("userId") REFERENCES public."User"(id);


--
-- TOC entry 3494 (class 2606 OID 18081)
-- Name: UserSprint fk_userid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."UserSprint"
    ADD CONSTRAINT fk_userid FOREIGN KEY ("userId") REFERENCES public."User"(id);


--
-- TOC entry 3473 (class 2606 OID 18086)
-- Name: IssueLink fromIssue_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."IssueLink"
    ADD CONSTRAINT "fromIssue_fk" FOREIGN KEY ("fromIssueId") REFERENCES public."Issue"(id);


--
-- TOC entry 3474 (class 2606 OID 18091)
-- Name: IssueLink toIssue_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."IssueLink"
    ADD CONSTRAINT "toIssue_fk" FOREIGN KEY ("toIssueId") REFERENCES public."Issue"(id);


-- Completed on 2024-06-24 11:34:23

--
-- PostgreSQL database dump complete
--

