--
-- PostgreSQL database dump
--

-- Dumped from database version 16.0
-- Dumped by pg_dump version 16.0

-- Started on 2024-01-14 20:32:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 14 (class 2615 OID 50783)
-- Name: public; Type: SCHEMA; Schema: -; Owner: pg_database_owner
--

--
-- TOC entry 5100 (class 0 OID 0)
-- Dependencies: 14
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: pg_database_owner
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 903 (class 1247 OID 50785)
-- Name: authorization_role; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.authorization_role AS ENUM (
    'user',
    'admin'
);


ALTER TYPE public.authorization_role OWNER TO postgres;

--
-- TOC entry 906 (class 1247 OID 50790)
-- Name: domain_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.domain_type AS ENUM (
    'service',
    'circle',
    'role',
    'organization'
);


ALTER TYPE public.domain_type OWNER TO postgres;

--
-- TOC entry 909 (class 1247 OID 50800)
-- Name: event_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.event_type AS ENUM (
    'planning',
    'review',
    'retrospective',
    'management',
    'choiceForRole'
);


ALTER TYPE public.event_type OWNER TO postgres;

--
-- TOC entry 912 (class 1247 OID 50812)
-- Name: history_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.history_type AS ENUM (
    'create',
    'update',
    'newExecutor',
    'executorStartedWork',
    'newSprints'
);


ALTER TYPE public.history_type OWNER TO postgres;

--
-- TOC entry 915 (class 1247 OID 50824)
-- Name: link_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.link_type AS ENUM (
    'consist',
    'delegate'
);


ALTER TYPE public.link_type OWNER TO postgres;

--
-- TOC entry 918 (class 1247 OID 50830)
-- Name: proposal_status; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.proposal_status AS ENUM (
    'new',
    'accepted',
    'holded',
    'rejected'
);


ALTER TYPE public.proposal_status OWNER TO postgres;

--
-- TOC entry 921 (class 1247 OID 50840)
-- Name: situation_description_status; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.situation_description_status AS ENUM (
    'accepted',
    'notAccepted',
    'consideration'
);


ALTER TYPE public.situation_description_status OWNER TO postgres;

--
-- TOC entry 924 (class 1247 OID 50848)
-- Name: tension_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.tension_type AS ENUM (
    'tension',
    'driver'
);


ALTER TYPE public.tension_type OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 239 (class 1259 OID 50853)
-- Name: Comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Comment" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    description character varying(255),
    "authorUserId" uuid,
    "issueId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."Comment" OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 50859)
-- Name: Domain; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Domain" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    "domainType" public.domain_type,
    "isOpen" boolean,
    "mainDriverId" uuid,
    "parentDomainId" uuid,
    "implementationStrategy" character varying(255),
    restrictions character varying(255),
    "performanceCriteria" character varying(255),
    description character varying(255),
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."Domain" OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 50867)
-- Name: Event; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Event" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    "domainId" uuid,
    type public.event_type,
    "startDate" timestamp without time zone,
    "endDate" timestamp without time zone,
    "facilitatorUserId" uuid,
    "planningEventId" uuid,
    "reviewEventId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."Event" OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 50873)
-- Name: EventParticipant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."EventParticipant" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "eventId" uuid,
    "userId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."EventParticipant" OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 50879)
-- Name: History; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."History" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    description character varying(255),
    date timestamp without time zone,
    type public.history_type,
    "issueId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."History" OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 50887)
-- Name: Issue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Issue" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "domainId" uuid,
    "authorUserId" uuid,
    name character varying(255),
    description character varying(255),
    "tensionId" uuid,
    "executorUserId" uuid,
    "sprintId" uuid,
    "issueTypeId" uuid,
    "stageId" uuid,
    version integer,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    priority integer
);


ALTER TABLE public."Issue" OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 51210)
-- Name: IssueLink; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."IssueLink" (
    "fromIssueId" uuid NOT NULL,
    "toIssueId" uuid NOT NULL,
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "deletedAt" time without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "isDeleted" boolean,
    "updatedAt" timestamp without time zone
);


ALTER TABLE public."IssueLink" OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 50895)
-- Name: IssueType; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."IssueType" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    description character varying(255),
    "domainId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."IssueType" OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 50903)
-- Name: Proposal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Proposal" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "authorUserId" uuid,
    status public.proposal_status,
    description character varying(255),
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."Proposal" OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 50909)
-- Name: SituationDescription; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SituationDescription" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    description character varying(255),
    status public.situation_description_status,
    "domainId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "mongoId" uuid
);


ALTER TABLE public."SituationDescription" OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 50915)
-- Name: Sprint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Sprint" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    goal character varying(255),
    "startDate" timestamp without time zone,
    "endDate" timestamp without time zone,
    "planningEventId" uuid,
    "reviewEventId" uuid,
    "domainId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."Sprint" OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 50923)
-- Name: Stage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Stage" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    "domainId" uuid,
    "IssueTypeId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."Stage" OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 50929)
-- Name: StageLink; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."StageLink" (
    "previousStageId" uuid NOT NULL,
    "nextStageId" uuid NOT NULL,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."StageLink" OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 50934)
-- Name: Tension; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Tension" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "authorUserId" uuid,
    "tensionType" public.tension_type,
    "isPositive" boolean,
    description character varying(255),
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" time without time zone DEFAULT now()
);


ALTER TABLE public."Tension" OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 50940)
-- Name: TensionProposal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."TensionProposal" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "tensionId" uuid NOT NULL,
    "proposalId" uuid NOT NULL,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."TensionProposal" OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 50946)
-- Name: User; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."User" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    name character varying(255),
    email character varying(255),
    password character varying(255),
    "domainId" uuid,
    "resetPasswordExpires" timestamp without time zone,
    "resetPasswordCode" character varying(255),
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now(),
    "confirmEmailCode" character varying(255),
    "confirmEmail" character varying(255),
    "confirmEmailExpires" timestamp without time zone
);


ALTER TABLE public."User" OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 50954)
-- Name: UserDomainLink; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."UserDomainLink" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "userId" uuid,
    "domainId" uuid,
    "delegateDomainId" uuid,
    "linkType" public.link_type,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."UserDomainLink" OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 50960)
-- Name: UserSprint; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."UserSprint" (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "userId" uuid,
    "sprintId" uuid,
    "isDeleted" boolean DEFAULT false,
    "deletedAt" timestamp without time zone,
    "updatedAt" timestamp without time zone,
    "createdAt" timestamp without time zone DEFAULT now()
);


ALTER TABLE public."UserSprint" OWNER TO postgres;

--
-- TOC entry 4878 (class 2606 OID 50966)
-- Name: Comment Comment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Comment"
    ADD CONSTRAINT "Comment_pkey" PRIMARY KEY (id);


--
-- TOC entry 4880 (class 2606 OID 50968)
-- Name: Domain Domain_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Domain"
    ADD CONSTRAINT "Domain_pkey" PRIMARY KEY (id);


--
-- TOC entry 4884 (class 2606 OID 50970)
-- Name: EventParticipant EventParticipant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EventParticipant"
    ADD CONSTRAINT "EventParticipant_pkey" PRIMARY KEY (id);


--
-- TOC entry 4882 (class 2606 OID 50972)
-- Name: Event Event_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT "Event_pkey" PRIMARY KEY (id);


--
-- TOC entry 4886 (class 2606 OID 50974)
-- Name: History History_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."History"
    ADD CONSTRAINT "History_pkey" PRIMARY KEY (id);


--
-- TOC entry 4912 (class 2606 OID 51215)
-- Name: IssueLink IssueLink_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."IssueLink"
    ADD CONSTRAINT "IssueLink_pkey" PRIMARY KEY (id);


--
-- TOC entry 4890 (class 2606 OID 50976)
-- Name: IssueType IssueType_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."IssueType"
    ADD CONSTRAINT "IssueType_pkey" PRIMARY KEY (id);


--
-- TOC entry 4888 (class 2606 OID 50978)
-- Name: Issue Issue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT "Issue_pkey" PRIMARY KEY (id);


--
-- TOC entry 4892 (class 2606 OID 50980)
-- Name: Proposal Proposal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proposal"
    ADD CONSTRAINT "Proposal_pkey" PRIMARY KEY (id);


--
-- TOC entry 4894 (class 2606 OID 50982)
-- Name: SituationDescription SituationDescription_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SituationDescription"
    ADD CONSTRAINT "SituationDescription_pkey" PRIMARY KEY (id);


--
-- TOC entry 4896 (class 2606 OID 50984)
-- Name: Sprint Sprint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT "Sprint_pkey" PRIMARY KEY (id);


--
-- TOC entry 4900 (class 2606 OID 50986)
-- Name: StageLink StageLink_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."StageLink"
    ADD CONSTRAINT "StageLink_pkey" PRIMARY KEY ("previousStageId", "nextStageId");


--
-- TOC entry 4898 (class 2606 OID 50988)
-- Name: Stage Stage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Stage"
    ADD CONSTRAINT "Stage_pkey" PRIMARY KEY (id);


--
-- TOC entry 4904 (class 2606 OID 50990)
-- Name: TensionProposal TensionProposal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TensionProposal"
    ADD CONSTRAINT "TensionProposal_pkey" PRIMARY KEY ("tensionId", "proposalId");


--
-- TOC entry 4908 (class 2606 OID 50992)
-- Name: UserDomainLink UserDomainLink_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserDomainLink"
    ADD CONSTRAINT "UserDomainLink_pkey" PRIMARY KEY (id);


--
-- TOC entry 4910 (class 2606 OID 50994)
-- Name: UserSprint UserSprint_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserSprint"
    ADD CONSTRAINT "UserSprint_pkey" PRIMARY KEY (id);


--
-- TOC entry 4906 (class 2606 OID 50996)
-- Name: User User_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (id);


--
-- TOC entry 4902 (class 2606 OID 50998)
-- Name: Tension tension_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tension"
    ADD CONSTRAINT tension_pkey PRIMARY KEY (id);


--
-- TOC entry 4941 (class 2606 OID 50999)
-- Name: Tension fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Tension"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 4932 (class 2606 OID 51004)
-- Name: Proposal fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Proposal"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 4913 (class 2606 OID 51014)
-- Name: Comment fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Comment"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 4924 (class 2606 OID 51250)
-- Name: Issue fk_authoruserid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_authoruserid FOREIGN KEY ("authorUserId") REFERENCES public."User"(id);


--
-- TOC entry 4945 (class 2606 OID 51019)
-- Name: UserDomainLink fk_delegatedomainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserDomainLink"
    ADD CONSTRAINT fk_delegatedomainid FOREIGN KEY ("delegateDomainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4946 (class 2606 OID 51024)
-- Name: UserDomainLink fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserDomainLink"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4944 (class 2606 OID 51029)
-- Name: User fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4931 (class 2606 OID 51034)
-- Name: IssueType fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."IssueType"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4937 (class 2606 OID 51044)
-- Name: Stage fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Stage"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4917 (class 2606 OID 51049)
-- Name: Event fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4933 (class 2606 OID 51054)
-- Name: SituationDescription fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SituationDescription"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4934 (class 2606 OID 51059)
-- Name: Sprint fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4925 (class 2606 OID 51255)
-- Name: Issue fk_domainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_domainid FOREIGN KEY ("domainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4921 (class 2606 OID 51064)
-- Name: EventParticipant fk_eventid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EventParticipant"
    ADD CONSTRAINT fk_eventid FOREIGN KEY ("eventId") REFERENCES public."Event"(id);


--
-- TOC entry 4926 (class 2606 OID 51260)
-- Name: Issue fk_executoruserid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_executoruserid FOREIGN KEY ("executorUserId") REFERENCES public."User"(id);


--
-- TOC entry 4918 (class 2606 OID 51074)
-- Name: Event fk_facilitatoruserid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT fk_facilitatoruserid FOREIGN KEY ("facilitatorUserId") REFERENCES public."User"(id);


--
-- TOC entry 4923 (class 2606 OID 51079)
-- Name: History fk_issueid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."History"
    ADD CONSTRAINT fk_issueid FOREIGN KEY ("issueId") REFERENCES public."Issue"(id);


--
-- TOC entry 4914 (class 2606 OID 51084)
-- Name: Comment fk_issueid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Comment"
    ADD CONSTRAINT fk_issueid FOREIGN KEY ("issueId") REFERENCES public."Issue"(id);


--
-- TOC entry 4938 (class 2606 OID 51094)
-- Name: Stage fk_issuetypeid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Stage"
    ADD CONSTRAINT fk_issuetypeid FOREIGN KEY ("IssueTypeId") REFERENCES public."IssueType"(id);


--
-- TOC entry 4927 (class 2606 OID 51265)
-- Name: Issue fk_issuetypeid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_issuetypeid FOREIGN KEY ("issueTypeId") REFERENCES public."IssueType"(id);


--
-- TOC entry 4915 (class 2606 OID 51099)
-- Name: Domain fk_maindriverid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Domain"
    ADD CONSTRAINT fk_maindriverid FOREIGN KEY ("mainDriverId") REFERENCES public."Tension"(id);


--
-- TOC entry 4939 (class 2606 OID 51104)
-- Name: StageLink fk_nextstageid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."StageLink"
    ADD CONSTRAINT fk_nextstageid FOREIGN KEY ("nextStageId") REFERENCES public."Stage"(id);


--
-- TOC entry 4916 (class 2606 OID 51109)
-- Name: Domain fk_parentdomainid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Domain"
    ADD CONSTRAINT fk_parentdomainid FOREIGN KEY ("parentDomainId") REFERENCES public."Domain"(id);


--
-- TOC entry 4919 (class 2606 OID 51114)
-- Name: Event fk_planningeventid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT fk_planningeventid FOREIGN KEY ("planningEventId") REFERENCES public."Event"(id);


--
-- TOC entry 4935 (class 2606 OID 51119)
-- Name: Sprint fk_planningeventid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT fk_planningeventid FOREIGN KEY ("planningEventId") REFERENCES public."Event"(id);


--
-- TOC entry 4940 (class 2606 OID 51124)
-- Name: StageLink fk_previousstageid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."StageLink"
    ADD CONSTRAINT fk_previousstageid FOREIGN KEY ("previousStageId") REFERENCES public."Stage"(id);


--
-- TOC entry 4942 (class 2606 OID 51129)
-- Name: TensionProposal fk_proposalid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TensionProposal"
    ADD CONSTRAINT fk_proposalid FOREIGN KEY ("proposalId") REFERENCES public."Proposal"(id);


--
-- TOC entry 4920 (class 2606 OID 51134)
-- Name: Event fk_revieweventid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Event"
    ADD CONSTRAINT fk_revieweventid FOREIGN KEY ("reviewEventId") REFERENCES public."Event"(id);


--
-- TOC entry 4936 (class 2606 OID 51139)
-- Name: Sprint fk_revieweventid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Sprint"
    ADD CONSTRAINT fk_revieweventid FOREIGN KEY ("reviewEventId") REFERENCES public."Event"(id);


--
-- TOC entry 4948 (class 2606 OID 51149)
-- Name: UserSprint fk_sprintid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserSprint"
    ADD CONSTRAINT fk_sprintid FOREIGN KEY ("sprintId") REFERENCES public."Sprint"(id);


--
-- TOC entry 4928 (class 2606 OID 51270)
-- Name: Issue fk_sprintid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_sprintid FOREIGN KEY ("sprintId") REFERENCES public."Sprint"(id);


--
-- TOC entry 4929 (class 2606 OID 51275)
-- Name: Issue fk_stageid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_stageid FOREIGN KEY ("stageId") REFERENCES public."Stage"(id);


--
-- TOC entry 4943 (class 2606 OID 51159)
-- Name: TensionProposal fk_tensionid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TensionProposal"
    ADD CONSTRAINT fk_tensionid FOREIGN KEY ("tensionId") REFERENCES public."Tension"(id);


--
-- TOC entry 4930 (class 2606 OID 51280)
-- Name: Issue fk_tensionid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Issue"
    ADD CONSTRAINT fk_tensionid FOREIGN KEY ("tensionId") REFERENCES public."Tension"(id);


--
-- TOC entry 4947 (class 2606 OID 51169)
-- Name: UserDomainLink fk_userid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserDomainLink"
    ADD CONSTRAINT fk_userid FOREIGN KEY ("userId") REFERENCES public."User"(id);


--
-- TOC entry 4922 (class 2606 OID 51174)
-- Name: EventParticipant fk_userid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."EventParticipant"
    ADD CONSTRAINT fk_userid FOREIGN KEY ("userId") REFERENCES public."User"(id);


--
-- TOC entry 4949 (class 2606 OID 51179)
-- Name: UserSprint fk_userid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."UserSprint"
    ADD CONSTRAINT fk_userid FOREIGN KEY ("userId") REFERENCES public."User"(id);


--
-- TOC entry 4950 (class 2606 OID 51216)
-- Name: IssueLink fromIssue_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."IssueLink"
    ADD CONSTRAINT "fromIssue_fk" FOREIGN KEY ("fromIssueId") REFERENCES public."Issue"(id);


--
-- TOC entry 4951 (class 2606 OID 51221)
-- Name: IssueLink toIssue_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."IssueLink"
    ADD CONSTRAINT "toIssue_fk" FOREIGN KEY ("toIssueId") REFERENCES public."Issue"(id);


--
-- TOC entry 5101 (class 0 OID 0)
-- Dependencies: 14
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: pg_database_owner
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;


-- Completed on 2024-01-14 20:32:20

--
-- PostgreSQL database dump complete
--

