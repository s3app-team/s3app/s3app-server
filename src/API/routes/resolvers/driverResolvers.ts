import { DriverRepositories } from '../../../data/repositories/driverRepositories';
import { IEntityId } from '../../../types/IEntityId';
import { IDriver } from '../../../types/IDriver';
import { IUser } from '../../../types/IUser';
import { BaseResolver } from './baseResolvers';
import { ITension } from '../../../types/ITension';
import { IDomain } from '../../../types/IDomain';
import graphqlFieldEnumType from './GraphqlFieldEnumType';
import { IComment } from '../../../types/IComment';

// export const driverResolvers = () => {

//   return {
//     Query: {
//       getDrivers: () => DriverService.getAll(),
//       getDriver: async (_: object, args: { id: string }, context: { user: IDriver }) => {
//         const driver = await DriverService.getData(args.id);
//         if (driver && !await DomainHierarchy.checkUserDomain(context.user.id, driver.domainId)) {
//           throw new Error('Access denied');
//         }
//         return driver;
//       },
//     },
//     Mutation: {
//       createDriver: resolver<{ driver: IDriver }, Promise<IEntityId>>(
//         async (_, args, context) => {
//           if (args.driver.domainId && !await DomainHierarchy.checkUserDomain(args.driver.domainId, context.user.id)) {
//             throw new Error('Access denied');
//           }
//           return DriverService.create(args.driver);
//         },
//       ),
//       deleteDriver: resolver<{ id: string }, Promise<Boolean>>(
//         async (_, args, context) => {
//           if (!await DomainHierarchy.checkUserDriver(context.user.id, args.id)) {
//             throw new Error('Access denied');
//           }
//           return DriverService.delete(args.id);
//         },
//       ),
//       editDriver: resolver<{ id: string, driver: IDriver }, Promise<IDriver>>(
//         async (_, args, context) => {
//           if (!await DomainHierarchy.checkUserDriver(context.user.id, args.id)) {
//             throw new Error('Access denied');
//           }
//           if (args.driver.domainId && !await DomainHierarchy.checkUserDomain(context.user.id, args.driver.domainId)) {
//             throw new Error('Access denied');
//           }
//           return DriverService.edit(args.id, args.driver);
//         },
//       ),
//       unlinkDriverAndTensions: async (_: object, args: { driverId: string, tensionIds: string[] | string[] }) => DriverService.unlinkDriverAndTensions(args.driverId, args.tensionIds),
//       linkDriverAndTensions: async (_: object, args: { driverId: string, tensionIds: string[] }) => DriverService.linkDriverAndTensions(args.driverId, args.tensionIds),
//     },
//     Driver: {
//       // domain: async (driver: IDriver) => domainServices.getData(driver.domain)
//       authorUser: async (driver: IDriver) => driver.authorUserId ? DomainServices.getData(driver.authorUserId) : null,
//     },
//   };
// };

class DriverResolvers extends BaseResolver {
  setResolvers() {
    this.addQueryResolver<{ id: string }, IDriver[]>('getDrivers',
      async (args, user) => new DriverRepositories(user).getAll());

    this.addQueryResolver<{ domainId: string }, IDriver[]>('getDriversOfOrganization',
      async (args, user) => new DriverRepositories(user).getOfOrganization(args.domainId));

    this.addQueryResolver<{ id: string }, IDriver | null>('getDriver',
      async (args, user) => new DriverRepositories(user, args.id).getData());

    this.addMutationResolver<{ driver: IDriver }, IEntityId>('createDriver',
      async (args, user) => new DriverRepositories(user).create(args.driver));

    this.addMutationResolver<{ id: string }, boolean>('deleteDriver',
      async (args, user) => new DriverRepositories(user, args.id).delete());

    this.addMutationResolver<{ id: string, driver: IDriver }, IDriver>('editDriver',
      async (args, user) => new DriverRepositories(user, args.id).edit(args.driver));

    this.addMutationResolver<{ driverId: string, tensionIds: string[] | string[] }, boolean>('unlinkDriverAndTensions',
      async (args, user) => new DriverRepositories(user, args.driverId).unlinkDriverAndTensions(args.tensionIds));

    this.addMutationResolver<{ driverId: string, tensionIds: string[] }, IEntityId[]>('linkDriverAndTensions',
      async (args, user) => new DriverRepositories(user, args.driverId).linkDriverAndTensions(args.tensionIds));

    this.addEntityResolver<IDriver, IDomain | null>('Driver', 'domain',
      async (issue, user) => new DriverRepositories(user, issue.id).getDomain());

    this.addEntityResolver<IDriver, ITension[]>('Driver', 'tensions',
      async (driver, user) => new DriverRepositories(user, driver.id).getTensions());    

    this.addEntityResolver<IDriver, IUser | null>('Driver', 'authorUser',
      async (driver, user) => new DriverRepositories(user, driver.id).getAuthorUser());

    this.addEntityResolver<IDriver, IComment[]>(
      'Driver',
      'comments',
      async (issue, user) =>
        new DriverRepositories(user, issue.id).getComments(),
    );

    this.addScalarType(graphqlFieldEnumType('DriverEffect', ['+', '-', '0']));
  }
}

export const driverResolvers = new DriverResolvers();