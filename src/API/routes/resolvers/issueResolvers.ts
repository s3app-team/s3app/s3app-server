import { IssueFilter, IssueRepositories, IssueSort } from '../../../data/repositories/issueRepositories';
import { IDomain } from '../../../types/IDomain';
import { IDriver } from '../../../types/IDriver';
import { IEntityId } from '../../../types/IEntityId';
import { IIssue } from '../../../types/IIssue';
import { IIssueType } from '../../../types/IIssueType';
import { IStage } from '../../../types/IStage';
import { IUser } from '../../../types/IUser';
import { BaseResolver } from './baseResolvers';

class IssueResolvers extends BaseResolver {
  setResolvers() {
    this.setSchema(`
      input IssueFilter {
        name: String
        priority: Int
        stageName: String
        issueTypeName: String
        executorId: ID
        startDate: DateTime
        endDate: DateTime
        open: Boolean
        linkIssueId: ID
        domainId: ID
      }

      enum IssueSortField {
        createdAt
        name
        priority
        stage
        issueType
        author
        executor
        domain
        links
      }

      enum IssueSortDirection {
        asc
        desc
      }

      input IssueSort {
        field: IssueSortField!
        direction: IssueSortDirection!
      }

      type Query {
        getIssues(page: Int! perPage: Int! filter: IssueFilter sort: IssueSort): [Issue]!
      }

    `);
    this.addQueryResolver<{
      page: number,
      perPage: number,
      filter: IssueFilter,
      sort: IssueSort,
    }, IIssue[]>('getIssues', 
      async (args, user) => new IssueRepositories(user).getByFilters(args.page, args.perPage, args.filter, args.sort));

    this.addQueryResolver<{ id: string }, IIssue | null>('getIssue',
      async (args, user) => new IssueRepositories(user, args.id).getData());

    this.addQueryResolver<{ domainId: string }, IIssue[]>('getIssuesOfOrganization',
      async (args, user) => new IssueRepositories(user).getOfOrganization(args.domainId));

    this.addMutationResolver<{ issue: IIssue }, IEntityId>('createIssue',
      async (args, user) => new IssueRepositories(user).create(args.issue));

    this.addMutationResolver<{ id: string, issue: IIssue }, IIssue>('editIssue',
      async (args, user) => new IssueRepositories(user, args.id).edit(args.issue));

    this.addMutationResolver<{ issueId: string, tensionIds: string[] | string[] }, boolean>('unlinkIssueAndDrivers',
      async (args, user) => new IssueRepositories(user, args.issueId).unlinkIssueAndDrivers(args.tensionIds));

    this.addMutationResolver<{ issueId: string, tensionIds: string[] }, IEntityId[]>('linkIssueAndDrivers',
      async (args, user) => new IssueRepositories(user, args.issueId).linkIssueAndDrivers(args.tensionIds));

    this.addMutationResolver<{ id: string }, boolean>('deleteIssue',
      async (args, user) => new IssueRepositories(user, args.id).delete());

    this.addMutationResolver<{ id: string }, IIssue>('closeIssue',
      async (args, user) => new IssueRepositories(user, args.id).close());

    this.addMutationResolver<{ id: string }, IIssue>('openIssue',
      async (args, user) => new IssueRepositories(user, args.id).open());

    this.addEntityResolver<IIssue, IStage | null>('Issue', 'stage',
      async (issue, user) => new IssueRepositories(user, issue).getStage());

    this.addEntityResolver<IIssue, IIssueType | null>('Issue', 'issueType',
      async (issue, user) => new IssueRepositories(user, issue).getIssueType());

    this.addEntityResolver<IIssue, IDriver[]>('Issue', 'drivers',
      async (issue, user) => new IssueRepositories(user, issue.id).getDrivers());
      
    this.addEntityResolver<IIssue, IUser | null>('Issue', 'author',
      async (issue, user) => new IssueRepositories(user, issue.id).getAuthor());

    this.addEntityResolver<IIssue, IUser | null>('Issue', 'executor',
      async (issue, user) => new IssueRepositories(user, issue.id).getExecutor());

    this.addEntityResolver<IIssue, IDomain | null>('Issue', 'domain',
      async (issue, user) => new IssueRepositories(user, issue.id).getDomain());

    this.addEntityResolver<IIssue, IIssue[]>('Issue', 'issueLinks',
      async (issue, user) => new IssueRepositories(user, issue.id).getByLink());

    this.addEntityResolver<IIssue, IIssue[]>('Issue', 'comments',
      async (issue, user) => new IssueRepositories(user, issue.id).getComments());
  }
}

export const issueResolvers = new IssueResolvers();