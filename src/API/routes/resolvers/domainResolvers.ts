import { DomainRepositories } from '../../../data/repositories/domainRepositories';
import { InviteRepositories } from '../../../data/repositories/inviteRepositories';
import { IssueFilter, IssueSort } from '../../../data/repositories/issueRepositories';
import { OrganizationRepositories } from '../../../data/repositories/organizationRepositories';
import { UserRepositories } from '../../../data/repositories/userRepositories';
import config from '../../../libs/config';
import { IDomain } from '../../../types/IDomain';
import { IDriver } from '../../../types/IDriver';
import { IEntityId } from '../../../types/IEntityId';
import { IEvent } from '../../../types/IEvent';
import { IIssue } from '../../../types/IIssue';
import { IIssueType } from '../../../types/IIssueType';
import { IProposal } from '../../../types/IProposal';
import { ISprint } from '../../../types/ISprint';
import { IStage } from '../../../types/IStage';
import { ITension } from '../../../types/ITension';
import { IThought } from '../../../types/IThought';
import { IUser } from '../../../types/IUser';
import { IUserOrganizationInvite } from '../../../types/IUserOrganizationInvite';
import { AccessRole, IUserOrganizationLink } from '../../../types/IUserOrganizationLink';
import { BaseResolver } from './baseResolvers';

class DomainResolvers extends BaseResolver {
  setResolvers() {
    this.setSchema(`
      type Domain {
        id: ID
        subDomains: [Domain]
        isOrganization: Boolean
        name: String
        domainType: String
        stages: [Stage]
        parentDomain: Domain
        mainDriver: Driver
        implementationStrategy: String
        performanceCriteria: String
        restrictions: String
        issues(withSubDomains: Boolean page: Int perPage: Int filter: IssueFilter sort: IssueSort): [Issue]
        issuesCount(withSubDomains: Boolean filter: IssueFilter): Int
        proposals(withSubDomains: Boolean): [Proposal]
        tensions(withSubDomains: Boolean): [Tension]
        drivers(withSubDomains: Boolean): [Driver]
        sprints(withSubDomains: Boolean): [Sprint]
        events(withSubDomains: Boolean): [Event]
        thoughts(withSubDomains: Boolean): [Thought]
        issueTypes: [IssueType]
        participants: [User]
        description: String
    }
  `);
    this.addQueryResolver<{}, IDomain[]>('getDomains',
      async (args, user) => new DomainRepositories(user).getAll());

    this.addQueryResolver<{ id: string }, IDomain | null>('getDomain',
      async (args, user) => new DomainRepositories(user, args.id).getData());

    this.addQueryResolver<{ id: string }, (IUserOrganizationLink & { user: IUser })[]>(
      'getOrganizationUsers',
    async (args, user) => (await OrganizationRepositories.CreateFromDomain(user, args.id)).getOranizationUsers());

    this.addQueryResolver<{ domainId: string }, IDomain[]>(
      'getDomainsOfOrganization',
      async (args, user) => new DomainRepositories(user, args.domainId).getOfOrganization());

    this.addQueryResolver<{ domainId: string }, IUserOrganizationInvite[]>(
      'getInvites',
      async (args, user) => {
        const invites = await new InviteRepositories(user).getByDomainId(args.domainId);
        if (config.isDemo) {
          invites.forEach((invite) => {
            invite.email = '********';
          });
        }
        return invites;
      },
    );

    this.addMutationResolver<{ domain: IDomain }, IEntityId>('createDomain',
      async (args, user) => new DomainRepositories(user).create(args.domain));

    this.addMutationResolver<{ id: string, domain: IDomain }, IDomain>('editDomain',
      async (args, user) => new DomainRepositories(user, args.id).edit(args.domain));

    this.addMutationResolver<{ id: string }, boolean>('deleteDomain',
      async (args, user) => new DomainRepositories(user, args.id).delete());

    this.addMutationResolver<{ domainId: string; userIds: string[] }, IEntityId[]>('addUsersToDomain',
      async (args, user) => new DomainRepositories(user, args.domainId).addParticipants(args.userIds));

    this.addMutationResolver<{ domainId: string; userIds: string[] }, IEntityId>('removeUsersFromDomain',
      async (args, user) => new DomainRepositories(user, args.domainId).removeParticipants(args.userIds));

    this.addMutationResolver<{ subDomainIds: string[]; parentDomainId: string }, IEntityId>('addSubdomains',
      async (args, user) => new DomainRepositories(user, args.parentDomainId).addSubdomains(args.subDomainIds));

    this.addMutationResolver<{ subDomainIds: string[]; parentDomainId: string }, IEntityId>('removeSubdomains',
      async (args, user) => new DomainRepositories(user, args.parentDomainId).removeSubdomains(args.subDomainIds));

    // this.addMutationResolver
    // <{ domainId: string; userId: string, accessRole: AccessRole }, IEntityId>('addUserToOrganization',
    //   async (args, user) => 
    //     (await (await OrganizationRepositories.CreateFromDomain(user, args.domainId))
    //       .addUsersToOrganization([{ userId: args.userId, accessRole: args.accessRole }]))
    //       [0],
    // );

    this.addMutationResolver<{ domainId: string; userId: string, accessRole: AccessRole }, IEntityId>('changeUserRole',
      async (args, user) => 
        (await OrganizationRepositories.CreateFromDomain(user, args.domainId))
          .changeUserAccessRole(args.userId, args.accessRole),
    );

    this.addMutationResolver
    <{ domainId: string; userId: string }, IEntityId>('removeUserFromOrganization',
      async (args, user) =>
        ((await OrganizationRepositories.CreateFromDomain(user, args.domainId))
          .removeUsersFromOrganization([args.userId])),
    );

    this.addMutationResolver
    <{ domainId: string, name: string, email: string, license: string }, boolean>('sendInvite',
      async (args, user) =>
        (await OrganizationRepositories.CreateFromDomain(user, args.domainId))
          .sendInvite(args.name, args.email, args.license),
    );

    this.addMutationResolver
    <{ id: string }, boolean>('revokeInvite',
      async (args, user) => new InviteRepositories(user, args.id).delete());

    this.addEntityResolver<IDomain, IDomain | null>('Domain', 'parentDomain',
      async (domain, user) => domain.parentDomainId ? new DomainRepositories(user, domain.parentDomainId).getData() : null);

    this.addEntityResolver<IDomain, IDomain[]>('Domain', 'subDomains',
      async (domain, user) => new DomainRepositories(user).getByParentDomain(domain.id));

    this.addEntityResolver<IDomain, IStage[]>('Domain', 'stages',
      async (domain, user) => new DomainRepositories(user, domain).getStages());

    this.addEntityResolver<IDomain, IIssueType[]>('Domain', 'issueTypes',
      async (domain, user) => new DomainRepositories(user, domain).getIssueTypes());

    this.addEntityResolver<IDomain, IIssue[], { withSubDomains: boolean,
      page: number, perPage: number, filter: IssueFilter, sort: IssueSort,
    }>('Domain', 'issues',
      async (domain, user, args) => new DomainRepositories(user, domain)
        .getIssues(args.withSubDomains, args.page, args.perPage, args.filter, args.sort));

    this.addEntityResolver<IDomain, number, { withSubDomains: boolean,
      page: number, perPage: number, filter: IssueFilter, sort: IssueSort,
    }>('Domain', 'issuesCount',
      async (domain, user, args) => new DomainRepositories(user, domain)
        .getIssuesCount(args.withSubDomains, args.filter));

    this.addEntityResolver<IDomain, ISprint[], { withSubDomains: boolean }>('Domain', 'sprints',
      async (domain, user, args) => new DomainRepositories(user, domain).getSprints(args.withSubDomains));

    this.addEntityResolver<IDomain, IEvent[], { withSubDomains: boolean }>('Domain', 'events',
      async (domain, user, args) => new DomainRepositories(user, domain).getEvents(args.withSubDomains));

    this.addEntityResolver<IDomain, IThought[], { withSubDomains: boolean }>('Domain', 'thoughts',
      async (domain, user, args) => new DomainRepositories(user, domain).getThoughts(args.withSubDomains));

    this.addEntityResolver<IDomain, ITension[], { withSubDomains: boolean }>('Domain', 'tensions',
      async (domain, user, args) => new DomainRepositories(user, domain).getTensions(args.withSubDomains));

    this.addEntityResolver<IDomain, IProposal[], { withSubDomains: boolean }>('Domain', 'proposals',
      async (domain, user, args) => new DomainRepositories(user, domain).getProposals(args.withSubDomains));

    this.addEntityResolver<IDomain, IDriver[], { withSubDomains: boolean }>('Domain', 'drivers',
      async (domain, user, args) => new DomainRepositories(user, domain).getDrivers(args.withSubDomains));

    this.addEntityResolver<IDomain, IDriver | null>('Domain', 'mainDriver',
      async (domain, user) => new DomainRepositories(user, domain).getMainDriver());

    this.addEntityResolver<IDomain, IUser[]>('Domain', 'participants',
      async (domain, user) => new DomainRepositories(user, domain).getParticipants());

    this.addEntityResolver<IUserOrganizationInvite, IUser | undefined>('Invite', 'inviterUser',
      async (invite, user) => invite.inviterUserId ? new UserRepositories(user, invite.inviterUserId).getData() : undefined);

    this.addEntityResolver<IUserOrganizationInvite, IUser | undefined>('Invite', 'invitedUser',
      async (invite, user) => invite.invitedUserId ? new UserRepositories(user, invite.invitedUserId).getData() : undefined);

  }
}

export const domainResolvers = new DomainResolvers();