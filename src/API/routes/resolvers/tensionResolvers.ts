import { TensionRepositories } from '../../../data/repositories/tensionRepositories';
import { IComment } from '../../../types/IComment';
import { IDomain } from '../../../types/IDomain';
import { IEntityId } from '../../../types/IEntityId';
import { ITension } from '../../../types/ITension';
import { IUser } from '../../../types/IUser';
import graphqlFieldEnumType from './GraphqlFieldEnumType';
import { BaseResolver } from './baseResolvers';

class TensionResolvers extends BaseResolver {
  setResolvers(): void {
    this.setSchema(`
      scalar TensionMark

      type Tension {
        id: ID
        name: String
        description: String
        tensionMark: TensionMark
        authorUser: UserResDto
        domain: Domain
        createdAt: DateTime
        updatedAt: DateTime
        comments: [Comment]
     }

     input TensionInput {
        name: String
        description: String
        tensionMark: TensionMark
        domainId: ID
    }
    
      type Query {
        getTensionsOfOrganization(domainId: ID!): [Tension]!
        getTensions: [Tension]!
        getTension(id: ID!): Tension!
      }

      type Mutation {
        createTension(tension: TensionInput!): Tension
        editTension(id: ID!, tension: TensionInput): Tension
        deleteTension(id: ID!): Boolean
      }
    `);

    this.addQueryResolver<{}, ITension[]>('getTensions', (args, user) =>
      new TensionRepositories(user).getAll(),
    );

    this.addQueryResolver<{ domainId: string }, ITension[]>(
      'getTensionsOfOrganization',
      async (args, user) =>
        new TensionRepositories(user).getOfOrganization(args.domainId),
    );

    this.addQueryResolver<{ id: string }, ITension | null>(
      'getTension',
      async (args, user) =>
        new TensionRepositories(user, args.id).getData(),
    );

    this.addMutationResolver<{ tension: ITension }, IEntityId>(
      'createTension',
      async (args, user) =>
        new TensionRepositories(user).create(args.tension),
    );

    this.addMutationResolver<{ id: string }, boolean>(
      'deleteTension',
      async (args, user) =>
        new TensionRepositories(user, args.id).delete(),
    );

    this.addMutationResolver<
    { id: string; tension: ITension },
    ITension
    >('editTension', async (args, user) =>
      new TensionRepositories(user, args.id).edit(args.tension),
    );

    this.addEntityResolver<ITension, IDomain | null>(
      'Tension',
      'domain',
      async (issue, user) =>
        new TensionRepositories(user, issue.id).getDomain(),
    );

    this.addEntityResolver<ITension, IUser | null>(
      'Tension',
      'authorUser',
      async (tension, user) =>
        new TensionRepositories(user, tension).getAuthorUser(),
    );

    this.addEntityResolver<ITension, IComment[]>(
      'Tension',
      'comments',
      async (issue, user) =>
        new TensionRepositories(user, issue.id).getComments(),
    );

    this.addScalarType(
      graphqlFieldEnumType('TensionMark', ['+', '-', '0']),
    );
  }
}

export const tensionResolvers = new TensionResolvers();
