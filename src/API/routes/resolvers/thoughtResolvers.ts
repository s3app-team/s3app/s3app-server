import { ThoughtRepositories } from '../../../data/repositories/thoughtRepositories';
import { IComment } from '../../../types/IComment';
import { IDomain } from '../../../types/IDomain';
import { IEntityId } from '../../../types/IEntityId';
import { IThought } from '../../../types/IThought';
import { IUser } from '../../../types/IUser';
import { BaseResolver } from './baseResolvers';

class ThoughtResolvers extends BaseResolver {
  setResolvers(): void {
    this.addQueryResolver<{}, IThought[]>('getThoughts', (args, user) =>
      new ThoughtRepositories(user).getAll(),
    );

    this.addQueryResolver<{ domainId: string }, IThought[]>(
      'getThoughtsOfOrganization',
      async (args, user) =>
        new ThoughtRepositories(user).getOfOrganization(args.domainId),
    );

    this.addQueryResolver<{ id: string }, IThought | null>(
      'getThought',
      async (args, user) =>
        new ThoughtRepositories(user, args.id).getData(),
    );

    this.addMutationResolver<{ thought: IThought }, IEntityId>(
      'createThought',
      async (args, user) =>
        new ThoughtRepositories(user).create(args.thought),
    );

    this.addMutationResolver<{ id: string }, boolean>(
      'deleteThought',
      async (args, user) =>
        new ThoughtRepositories(user, args.id).delete(),
    );

    this.addMutationResolver<
    { id: string; thought: IThought },
    IThought
    >('editThought', async (args, user) =>
      new ThoughtRepositories(user, args.id).edit(args.thought),
    );

    this.addEntityResolver<IThought, IDomain | null>(
      'Thought',
      'domain',
      async (issue, user) =>
        new ThoughtRepositories(user, issue.id).getDomain(),
    );

    this.addEntityResolver<IThought, IUser | null>(
      'Thought',
      'authorUser',
      async (thought, user) =>
        new ThoughtRepositories(user, thought).getAuthorUser(),
    );

    this.addEntityResolver<IThought, IComment[]>(
      'Thought',
      'comments',
      async (issue, user) =>
        new ThoughtRepositories(user, issue.id).getComments(),
    );
  }
}

export const thoughtResolvers = new ThoughtResolvers();
