import { AuthRepositories } from '../../../data/repositories/authRepositories';
import { ConfirmEmailRepositories } from '../../../data/repositories/confirmEmailRepositories';
import { LicenseRepositories } from '../../../data/repositories/licenseRepositories';
import { ResetPasswordRepositories } from '../../../data/repositories/resetPasswordRepositories';
import { UserRepositories } from '../../../data/repositories/userRepositories';
import { IUserResDTO } from '../../../types/DTO/IUserResDTO';
import { IUser } from '../../../types/IUser';
import { BaseResolver } from './baseResolvers';

// export const authResolvers = () => {
//   return {
//     Mutation: {
//       signUp: resolver< { name: string; email: string; password: string, license: string }, Promise<IUser>>(
//         async (_, args) => AuthServices.signUp(args)),
//       changePassword: async (
//         _: object,
//         args: { oldPassword: string; newPassword: string },
//         contextValue: { user: IUser },
//       ) => {
//         return AuthServices.changePassword(
//           contextValue.user.id ?? '',
//           args.oldPassword,
//           args.newPassword,
//         );
//       },

//       signIn: async (
//         _: object,
//         args: { name: string; email: string; password: string },
//       ) => AuthServices.signIn(args),

//       changePasswordAfterRecover: async (
//         _: object,
//         args: { code: string; password: string },
//       ) =>
//         AuthServices.changePasswordAfterRecover(
//           args.code,
//           args.password,
//         ),

//       sendRecoverPasswordLink: async (
//         _: object,
//         args: { email: string },
//       ) => AuthServices.sendRecoverPasswordLink(args.email),

//       sendEmailConfirmationLink: async (
//         _: object,
//         args: { email: string },
//         contextValue: { user: IUser },
//       ) => AuthServices.sendEmailConfirmationLink(contextValue.user.id ?? '', args.email),

//       confirmEmail: async (
//         _: object,
//         args: { code: string },
//       ) => AuthServices.ConfirmEmail(args.code),

//       updateLicense: async (
//         _: object,
//         args: { license: string },
//         contextValue: { user: IUser },
//       ) => AuthServices.updateLicense(contextValue.user.id, args.license),
//     },
//     Query: {
//       checkResetPasswordCode: async (
//         _: object,
//         args: { code: string },
//       ) => AuthServices.checkResetPasswordCode(args.code),
//     },
//   };
// };

class AuthResolvers extends BaseResolver {
  setResolvers(): void {
    this.addQueryResolver<{ code: string }, IUserResDTO>('checkResetPasswordCode', 
      async (args, user) => new ResetPasswordRepositories(user).checkResetPasswordCode(args.code));

    this.addMutationResolver<{ name: string; email: string; password: string, license : string },
    Partial<IUser>>('signUp', 
      async (args, user) => new AuthRepositories(user).signUp(args.email, args.name, args.password, args.license));

    this.addMutationResolver
    <{ oldPassword: string; newPassword: string }, IUserResDTO>(
      'changePassword', 
      async (args, user) => new UserRepositories(user, user.id).changePassword(args.oldPassword, args.newPassword));

    this.addMutationResolver<{ email: string; password: string }, string>('signIn',
      async (args, user) => new AuthRepositories(user).signIn(args.email, args.password));
      
    this.addMutationResolver<{ code: string; password: string }, boolean>('changePasswordAfterRecover',
      async (args, user) => new ResetPasswordRepositories(user).changePasswordAfterRecover(args.code, args.password));
      
    this.addMutationResolver<{ email: string }, boolean>('sendRecoverPasswordLink',
      async (args, user) => new ResetPasswordRepositories(user).sendRecoverPasswordLink(args.email));

    this.addMutationResolver<{ email: string }, boolean>('sendEmailConfirmationLink',
      async (args, user) => new ConfirmEmailRepositories(user, user.id).sendEmailConfirmationLink(args.email));

    this.addMutationResolver<{ code: string }, boolean>('confirmEmail',
      async (args, user) => new ConfirmEmailRepositories(user).ConfirmEmail(args.code));

    this.addMutationResolver<{ license: string }, boolean>('updateLicense',
      async (args, user) => new LicenseRepositories(user, user.id).updateLicense(args.license));
  }
}

export const authResolvers = new AuthResolvers();
