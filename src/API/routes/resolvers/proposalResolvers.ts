import { ProposalRepositories } from '../../../data/repositories/proposalRepositories';
import { IComment } from '../../../types/IComment';
import { IDomain } from '../../../types/IDomain';
import { IDriver } from '../../../types/IDriver';
import { IEntityId } from '../../../types/IEntityId';
import { IProposal } from '../../../types/IProposal';
import { IUser } from '../../../types/IUser';
import { BaseResolver } from './baseResolvers';

class ProposalResolvers extends BaseResolver {
  setResolvers() {
    this.setSchema(`
      type Query {
        getProposalsOfOrganization(domainId: ID!): [Proposal]!
        getProposals: [Proposal]!
        getProposal(id: ID!): Proposal!
      }

      type Mutation {
        createProposal(proposal: ProposalInput!): Proposal
        editProposal(id: ID!, proposal: ProposalInput!): Proposal
        deleteProposal(id: ID!): Boolean
        linkProposalAndDrivers(proposalId: ID, driverIds: [ID]): Proposal
        unlinkProposalAndDrivers(proposalId: ID, driverIds: [ID]): Boolean
      }

      enum ProposalStatus {
        new
        accepted
        holded
        rejected
        ceased
        review
      }

      type Proposal {
        id: ID
        name: String
        description: String
        authorUser: UserResDto
        status: ProposalStatus
        drivers: [Driver]
        domain: Domain
        createdAt: DateTime
        updatedAt: DateTime
        comments: [Comment]
      
      }
      
      input ProposalInput {
          name: String
          description: String
          status: ProposalStatus
          driverIds: [ID]
          domainId: ID
      }
    `);

    this.addQueryResolver<{ id: string }, IProposal[]>('getProposals',
      async (args, user) => new ProposalRepositories(user).getAll());

    this.addQueryResolver<{ domainId: string }, IProposal[]>('getProposalsOfOrganization',
      async (args, user) => new ProposalRepositories(user).getOfOrganization(args.domainId));

    this.addQueryResolver<{ id: string }, IProposal | null>('getProposal',
      async (args, user) => new ProposalRepositories(user, args.id).getData());

    this.addMutationResolver<{ proposal: IProposal }, IEntityId>('createProposal',
      async (args, user) => new ProposalRepositories(user).create(args.proposal));

    this.addMutationResolver<{ id: string }, boolean>('deleteProposal',
      async (args, user) => new ProposalRepositories(user, args.id).delete());

    this.addMutationResolver<{ id: string, proposal: IProposal }, IProposal>('editProposal',
      async (args, user) => new ProposalRepositories(user, args.id).edit(args.proposal));

    this.addMutationResolver<{ proposalId: string, tensionIds: string[] | string[] }, boolean>('unlinkProposalAndDrivers',
      async (args, user) => new ProposalRepositories(user, args.proposalId).unlinkProposalAndDrivers(args.tensionIds));

    this.addMutationResolver<{ proposalId: string, tensionIds: string[] }, IEntityId[]>('linkProposalAndDrivers',
      async (args, user) => new ProposalRepositories(user, args.proposalId).linkProposalAndDrivers(args.tensionIds));

    this.addEntityResolver<IProposal, IDomain | null>('Proposal', 'domain',
      async (proposal, user) => new ProposalRepositories(user, proposal.id).getDomain());

    this.addEntityResolver<IProposal, IDriver[]>('Proposal', 'drivers',
      async (proposal, user) => new ProposalRepositories(user, proposal.id).getDrivers());

    this.addEntityResolver<IProposal, IComment[]>(
      'Proposal',
      'comments',
      async (issue, user) =>
        new ProposalRepositories(user, issue.id).getComments(),
    );

    this.addEntityResolver<IProposal, IUser | null>('Proposal', 'authorUser',
      async (proposal, user) => new ProposalRepositories(user, proposal.id).getAuthorUser());
  }
}

export const proposalResolvers = new ProposalResolvers();