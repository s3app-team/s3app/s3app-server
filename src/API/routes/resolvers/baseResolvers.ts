import { GraphQLScalarType } from 'graphql';
import { IUser } from '../../../types/IUser';

export type Resolver<TArguments, TResult> = (parent: object, args:TArguments, contextValue: { user: IUser }, info: object) => TResult | Promise<TResult>;

export const resolver = <TArguments, TResult>(func:Resolver<TArguments, TResult>) => func;

export abstract class BaseResolver {
  resolvers: { 
    [type: string]: { [field: string]: (parent: any, args: any, contextValue: { user: IUser }, info: any) => Promise<any> } | GraphQLScalarType | any,
  } = {};

  schema: string = '';

  constructor() {
    this.setResolvers();
  }

  addResolver<TArguments, TResult, Parent = object>(
    type: string,
    field: string,
    func:(parent: Parent, args: TArguments, user: IUser, info: object) => Promise<TResult>) {
    if (!this.resolvers[type]) {
      this.resolvers[type] = {};
    }
    this.resolvers[type][field] = (parent: any, args: any, contextValue: { user: IUser }, info: object) => {
      return func(parent, args, contextValue.user, info);
    };
  }

  addQueryResolver<TArguments, TResult>(
    field: string,
    func:(args: TArguments, user: IUser, info: object) => Promise<TResult>) {
    this.addResolver('Query', field, (parent, args: TArguments, user, info) => func(args, user, info));
  }

  addMutationResolver<TArguments, TResult>(
    field: string,
    func:(args: TArguments, user: IUser, info: object) => Promise<TResult>) {
    this.addResolver('Mutation', field, (parent, args: TArguments, user, info) => func(args, user, info));
  }

  addEntityResolver<TEntity, TResult, TArguments = null>(
    entity: string,
    field: (keyof TEntity & string) | (string & {}),
    func:(parent: TEntity, user: IUser, args: TArguments, info: object) => Promise<TResult>) {
    this.addResolver(entity, field, (parent: TEntity, args: TArguments, user, info) => func(parent, user, args, info));
  }

  addScalarType(type: GraphQLScalarType) {
    this.resolvers[type.name] = type;
  }

  abstract setResolvers(): void;

  getResolvers() {
    return this.resolvers;
  }

  setSchema(schema: string) {
    this.schema = schema;
  }

  getSchema() {
    return this.schema;
  }
}