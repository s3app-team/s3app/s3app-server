import { CommentRepositories } from '../../../data/repositories/commentRepositories';
import { IComment } from '../../../types/IComment';
import { IUser } from '../../../types/IUser';
import { BaseResolver } from './baseResolvers';

// export const commentResolvers = () => {

//   return {
//     Query: {
//       getComments: () => CommentServices.getAll(),
//       getComment: async (_: object, args: { id: string }) => CommentServices.getData(args.id),

//     },
//     Mutation: {
//       createComment: async (_: object, args: { comment: IComment }) => CommentServices.createComment(args.comment),
//       editComment: async (_: object, args: { id: string, comment: IComment }) => CommentServices.edit(args.id, args.comment),
//       deleteComment: async (_: object, args: { id: string }) => CommentServices.delete(args.id),
//     },
//     Comment: {
//       author: async (comment: IComment) => {
//         return UserServices.getData(comment.authorUserId);
//       },
//     },
//   };
// };

class CommentResolvers extends BaseResolver {
  setResolvers() {
    // this.addQueryResolver<{}, IComment[]>('getComments', 
    //   async (args, user) => new CommentRepositories(user).getAll());

    // this.addQueryResolver<{ id: string }, IComment>('getComment', 
    //   async (args, user) => new CommentRepositories(user, args.id).getData());

    this.addMutationResolver<{ comment: IComment }, IComment>('createComment',
      async (args, user) => new CommentRepositories(user).create(args.comment));

    this.addMutationResolver<{ id: string, comment: IComment }, IComment>('editComment',
      async (args, user) => new CommentRepositories(user, args.id).edit(args.comment));

    this.addMutationResolver<{ id: string }, boolean>('deleteComment',
      async (args, user) => new CommentRepositories(user, args.id).delete());

    this.addEntityResolver<IComment, IUser | null>('Comment', 'author',
      async (comment, user) => new CommentRepositories(user, comment.id).getAuthor());
  }
}

export const commentResolvers = new CommentResolvers();