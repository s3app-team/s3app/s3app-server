// eslint-disable-next-line import/no-extraneous-dependencies
import express from 'express';
import config from './libs/config';

import { ApolloServer, gql } from 'apollo-server-express';
import path from 'path';
import jwt from 'jsonwebtoken';
import { CronJob } from 'cron';
import { userResolvers } from './API/routes/resolvers/userResolvers';
import { issueResolvers } from './API/routes/resolvers/issueResolvers';
import { commentResolvers } from './API/routes/resolvers/commentResolvers';
import { domainResolvers } from './API/routes/resolvers/domainResolvers';
import { eventResolvers } from './API/routes/resolvers/eventResolvers';
import { historyResolvers } from './API/routes/resolvers/historyResolvers';
import { sprintResolvers } from './API/routes/resolvers/sprintResolvers';
import { stageResolvers } from './API/routes/resolvers/stageResolvers';
import { proposalResolvers } from './API/routes/resolvers/proposalResolvers';
import { tensionResolvers } from './API/routes/resolvers/tensionResolvers';
import { authResolvers } from './API/routes/resolvers/authResolvers';
import { IUser } from './types/IUser';
import { AuthRepositories } from './data/repositories/authRepositories';
import { LicenseRepositories } from './data/repositories/licenseRepositories';
import { driverResolvers } from './API/routes/resolvers/driverResolvers';
import { thoughtResolvers } from './API/routes/resolvers/thoughtResolvers';
import { BaseResolver } from './API/routes/resolvers/baseResolvers';
import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';
import { readFileSync } from 'fs';
import { issueTypeResolvers } from './API/routes/resolvers/issueTypeResolvers';

const PORT = config.port;
const typeDefs = readFileSync(
  path.resolve(__dirname, './API/schema/api.graphql'),
).toString('utf-8');

let resolvers:BaseResolver[] = [
  issueResolvers,
  userResolvers,
  commentResolvers,
  domainResolvers,
  eventResolvers,
  historyResolvers,
  sprintResolvers,
  stageResolvers,
  proposalResolvers,
  tensionResolvers,
  authResolvers,
  driverResolvers,
  thoughtResolvers,
  issueTypeResolvers,
];

async function getUser(token: string) {
  if (token) {
    try {
      const tokenObj = jwt.verify(token, config.jwt.secret_key);
      if (typeof tokenObj === 'object') {
        const user = (await new AuthRepositories(undefined, tokenObj.id).getData());
        user.id = tokenObj.id;
        return user;
      }
    } catch (err) {
      throw new Error('Session invalid');
    }
  }
}

interface MyFragmentDefenition {
  operation: string
}


const apolloServer = new ApolloServer({
  typeDefs: [typeDefs, ...resolvers.map(resolver => resolver.getSchema())],
  resolvers: resolvers.map(resolver => resolver.resolvers),
  introspection: true,
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
  context: async ({ req }) => {
    const token = req.headers.authorization;
    // const operation = (
    //   gql`
    //             ${req.body.query}
    //         `.definitions[0] as any
    // ).selectionSet.selections[0].name.value;

    const definitions:readonly MyFragmentDefenition[] = (gql`
      ${req.body.query}
    `.definitions as readonly MyFragmentDefenition[]);

    const operations:string[] = [];

    (definitions).forEach((definition: any) => {
      if (definition.operation?.toLowerCase() === 'mutation' || definition.operation?.toLowerCase() === 'query') {
        definition.selectionSet.selections.forEach((selection: any) => {
          operations.push(selection.name.value);
        });
      }
    });

    if (config.isDemo) {
      definitions.forEach(type => {
        if (
          type.operation
                        && type.operation.toLowerCase() === 'mutation'
        ) {
          throw new Error('DEMO_ERROR');
        }
      });

      let user = (await new AuthRepositories(undefined, config.demoUser).getData()) as IUser;
      user.id = config.demoUser;
      return { user };
    } else if (token) {
      let user = await getUser(token);
      if (config.isLicense && (!user?.license || (user?.licenseExpires || 0) < new Date())) {
        operations.forEach(operation => {
          if (![
            'updateLicense', 
            'me',
            'checkResetPasswordCode',
            'changePasswordAfterRecover',
            'sendRecoverPasswordLink',
            'sendEmailConfirmationLink',
            'confirmEmail',
            'getDomains',
            'getDomain',
          ].includes(operation)) {
            throw new Error('LICENSE_ERROR');
          }
        });
      }
      return { user };
    }


    operations.forEach(operation => {
      if (
        ![
          'signIn',
          'signUp',
          '__schema',
          'checkResetPasswordCode',
          'changePasswordAfterRecover',
          'sendRecoverPasswordLink',
          'sendEmailConfirmationLink',
          'confirmEmail',
        ].includes(operation)
      ) {
        throw new Error('Session invalid');
      }
    });
  },
});

async function ServerStart() {
  const app = express();
  await apolloServer.start();

  apolloServer.applyMiddleware({ app: app });
  app.use(function (
    err: { message: any; status: any },
    req: { app: { get: (arg0: string) => string } },
    res: {
      locals: { message: any; error: any };
      status: (arg0: any) => void;
      render: (arg0: string) => void;
    },
    // next: any,
  ) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.render('error');
  });

  // await mongoose
  //     .connect(config.mongoose.uri, {
  //         // @ts-ignore
  //         useUnifiedTopology: true,
  //         useNewUrlParser: true,
  //     })
  //     .catch((err) => {
  //         console.log("Не удалось подключиться к БД");
  //         process.exit();
  //     });

  if (config.isLicense) {
    const job = new CronJob('0 * * * * *', async () => {
      await new LicenseRepositories(undefined).updateLicenses();
    });
    job.start();
  }

  app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
  });
}

ServerStart();
