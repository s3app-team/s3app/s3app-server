import { IEntity } from './IEntity';
import { AuthorizationRoles } from './enum/authorizationRoles';

export interface IUser extends IEntity {
  _id: string;
  id: string;
  name: string;
  email: string;
  password?: string;
  circles?: string[];
  domains?: string[];
  issues?: string[];
  roles?: string[];
  participantsInEvents?: string[];
  facilitatorInEvents?: string[];
  sprints?: string[];
  comments?: string[];
  authorizationRoles?: AuthorizationRoles;
  resetPasswordCode?: string;
  resetPasswordExpires?: Date;
  confirmEmail?: string;
  confirmEmailExpires?: Date;
  confirmEmailCode?: string;
  license?: string;
  licenseExpires?: Date;
}

