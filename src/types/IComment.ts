import { IEntity } from './IEntity';

export interface IComment extends IEntity {
  id: string;
  description: string;
  author: string;
  authorUserId: string;
  date: string;
  issueId?: string;
  tensionId?: string;
  driverId?: string;
  proposalId?: string;
  thoughtId?: string;
}

