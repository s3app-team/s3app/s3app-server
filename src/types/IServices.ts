export type ILicense = {
  id: string;
  purchase_id: string;
  license_key: string;
  expire_time: Date;
  create_time: Date;
  activated: boolean;
  activate_time: Date;
};