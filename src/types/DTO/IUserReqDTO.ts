import { AuthorizationRoles } from '../enum/authorizationRoles';

export interface IUpdateUser {
  id: string,
  name?: string,
  email?: string,
  password?: string,
  license?: string,
  licenseExpires?: Date,
  // circles?: string[],
  // domains?: string[],
  // issues?: string[],
  // roles?: string[],
  // participantsInEvents?: string[],
  // facilitatorInEvents?: string[],
  // sprints?: string[],
  // comments?: string[],
  updatedAt?: string;
  authorizationRoles?: AuthorizationRoles
  resetPasswordExpires?: Date
}