import { IEntity } from './IEntity';

export interface IThought extends IEntity {
  authorUserId: string,
  description: string,
  domainId: string,
}