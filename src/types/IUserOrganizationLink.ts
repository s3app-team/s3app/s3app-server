export enum AccessRole {
  user = 'user',
  admin = 'admin',
}

export interface IUserOrganizationLink {
  userId: string,
  domainId: string,
  accessRole: AccessRole,
}