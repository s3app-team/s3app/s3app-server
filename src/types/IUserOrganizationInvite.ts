import { IEntity } from './IEntity';

export interface IUserOrganizationInvite extends IEntity {
  email: string,
  name: string,
  license: string,
  organizationId: string,
  inviterUserId: string,
  invitedUserId: string,
  isAccepted: boolean,
  acceptedAt: Date,
}