export interface IStage {
  id: string,
  name: string,
  domainId: string,
  order: number,
  color: string,
  isStart: boolean,
  isEnd: boolean,
}