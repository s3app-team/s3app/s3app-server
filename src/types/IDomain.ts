import { IEntity } from './IEntity';

export enum DomainType {
  service = 'service',
  circle = 'circle',
  role = 'role',
  organization = 'organization',
}

export interface IDomain extends IEntity {
  id: string,
  name: string,
  isOrganization: boolean,
  domainType: DomainType,
  mainDriverId: string,
  parentDomainId: string,
  subDomains?: string[],
  stages: string[],
  implementationStrategy: string,
  performanceCriteria: string,
  restrictions: string,
  actualIssues: string[],
  proposals: string[],
  tensions: string[],
  sprints: string[],
  participants?: string[]
}