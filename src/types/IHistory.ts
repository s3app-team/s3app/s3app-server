import { IEntity } from './IEntity';

export enum HistoryType {
  'create' = 'create',
  'update' = 'update',
  'newExecutor' = 'newExecutor',
  'executorStartedWork' = 'executorStartedWork',
  'newSprints' = 'newSprints',
}

export interface IHistory extends IEntity {
  id: string,
  name: string,
  description: string,
  date?: string,
  type: HistoryType,
  issueId: string,
  mongoId: string,
}