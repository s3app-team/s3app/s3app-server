import config from '../libs/config';
import knex from 'knex';

const dbClient = knex(config.db);
  
export = dbClient;