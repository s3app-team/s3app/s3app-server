import { IUser } from '../../types/IUser';

export interface IUserMongo extends IUser {
  deleted: boolean
}