import { ISprint } from '../../types/ISprint';

export interface ISprintMongo extends ISprint {
  planningEvent: string;
  reviewEvent: string,
  domain: string,
  deleted: boolean,
  _id: string
}