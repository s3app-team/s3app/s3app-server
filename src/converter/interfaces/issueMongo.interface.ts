import { IIssue } from '../../types/IIssue';

export interface IIssueMongo extends IIssue {
  author: string,
  domain: string,
  executor: string,
  stage: string,
  sprint: string,
  issueLinks: string[],
  _id: string,
  deleted: string,
  dateCreated: string
}