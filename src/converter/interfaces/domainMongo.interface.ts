import { IDomain } from '../../types/IDomain';

export interface IDomainMongo extends IDomain {
  deleted: boolean,
  participants: string[],
  mainDomain: string,
  _id: string,
}