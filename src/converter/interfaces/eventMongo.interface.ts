import { IEvent } from '../../types/IEvent';

export interface IEventMongo extends IEvent {
  domain: string,
  _id: string,
}