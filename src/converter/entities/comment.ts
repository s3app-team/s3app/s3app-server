import { model, Schema, Types } from 'mongoose';
import mongoose_delete from 'mongoose-delete';
import { IComment } from '../../types/IComment';

const schema = new Schema({
  description: String,
  author: {
    type: Types.ObjectId,
    ref: 'user',
  },
  date: {
    type: Date,
    default: Date.now(),
  },
});

schema.plugin(mongoose_delete, { overrideMethods: 'all' });

export = model<IComment>('comment', schema);