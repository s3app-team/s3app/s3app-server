import { model, Schema, Types } from 'mongoose';
import { IEventMongo } from '../interfaces/eventMongo.interface';

const schema = new Schema({
  name: {
    type: String,
  },
  domain: {
    type: Types.ObjectId,
    ref: 'domain',
  },
  type: {
    type: String,
    enum: [
      'planning',
      'review',
      'retrospective',
      'management',
      'choiceForRole',
    ],
  },
  startDate: {
    type: Date,
  },
  endDate: {
    type: Date,
  },
  participants: [{
    type: Types.ObjectId,
    ref: 'user',
  }],
  facilitator: {
    type: Types.ObjectId,
    ref: 'user',
  },
  planningEvent: {
    type: Types.ObjectId,
    ref: 'event',
  },
  reviewEvent: {
    type: Types.ObjectId,
    ref: 'event',
  },
});


export = model<IEventMongo>('event', schema);