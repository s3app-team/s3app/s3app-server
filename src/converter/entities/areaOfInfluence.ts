import { Schema, Types } from 'mongoose';


const schema = new Schema({
  name: String,
  isOrganization: Boolean,
  domainType: String,
  mainDriver: {
    type: Types.ObjectId,
    ref: 'driver',
  },
  mainDomain: {
    type: Types.ObjectId,
    ref: 'domain',
  },
  // subDomains: [{
  //     type: Types.ObjectId,
  //     ref: "domain"
  // }],
  implementationStrategy: String,
  performanceCriteria: String,
  restrictions: String,
  participants: [{
    type: Types.ObjectId,
    ref: 'user',
  }],
  description: String,
  stages: [{
    type: Types.ObjectId,
    ref: 'stage',
  }],
});


export = schema;