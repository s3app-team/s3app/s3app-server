import { ApolloError } from 'apollo-server-express';

export class NotFoundError extends ApolloError {
  constructor(message: string, extensions?: Record<string, any>) {
    super(message, 'BAD_USER_INPUT', extensions);
  
    Object.defineProperty(this, 'name', { value: 'NotFoundError' });
  }
}