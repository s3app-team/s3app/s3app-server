import dbClient from '../../utils/knex';
import { NotFoundError } from '../erros';
import BaseRepository from './baseRepository';
import { TensionRepositories } from './tensionRepositories';
import { IUser } from '../../types/IUser';
import { UserRepositories } from './userRepositories';
import { IDriver } from '../../types/IDriver';
import { ITension } from '../../types/ITension';
import { IDomain } from '../../types/IDomain';
import { DomainRepositories } from './domainRepositories';
import { IEntityId } from '../../types/IEntityId';
import { CommentRepositories } from './commentRepositories';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type ThrowException<T = any> = never;


export class DriverRepositories extends BaseRepository<IDriver> {
  public async getComments() {
    return new CommentRepositories(this.contextUser).getByDriver(
      this.id as string,
    );
  }

  getTable(): string {
    return 'Driver';
  }

  public async getByDomain(domainId: string, withSubDomains: boolean) {
    if (withSubDomains) {
      const subdomains = (await this.domainHierarchy)?.findSubDomains(
        domainId,
      );
      return this.getByQuery(_knex => 
        _knex.whereIn(
          'domainId',
          subdomains?.map((domain) => domain.id) || [],
        ));
    }
    return this.getByFields({ domainId });
  }

  public async linkDriverAndTensions(tensionIds: string[]) {
    const tensions = await new TensionRepositories(this.contextUser).getByIds(tensionIds);
    if (!tensions || tensions.length !== tensionIds.length) {
      throw new NotFoundError('Tensions not found');
    }
    const driver = await this.getData();
    if (!driver) {
      throw new NotFoundError('Driver not found');
    }
    const insertData = tensionIds.map(tensionId => ({
      driverId: this.id,
      tensionId,
    }));
    if (insertData.length) {
      const insertedTensionLinks = await dbClient('TensionDriver')
        .insert(insertData)
        .returning('id');
      return insertedTensionLinks;
    }
    return [];
  }

  public async unlinkDriverAndTensions(tensionIds: string[] | string[]): Promise<boolean> {
    await this.getData();
    if (tensionIds.length) {
      await dbClient('TensionDriver')
        .delete()
        .where('driverId', this.id)
        .whereIn('tensionId', tensionIds);
      return true;
    }
    return true;
  }

  public async changeTensionLinks(tensionIds: string[]): Promise<IEntityId> {
    const data = await this.data;
    await this.checkEntitiesOfOrganization(tensionIds, data?.domainId as string);
    const currentTensions = await this.getTensions();
    const currentTensionLinksIds = currentTensions.map((tension) => tension.id);
    const toAdd = tensionIds.filter((issue) => !currentTensionLinksIds.includes(issue));
    const toDelete = currentTensionLinksIds.filter((tension) => !tensionIds.includes(tension));
    await this.linkDriverAndTensions(toAdd);
    await this.unlinkDriverAndTensions(toDelete);
    return { id: this.id };
  }

  public async create(newData: Partial<IDriver>): Promise<IDriver> {
    const { tensionIds } = newData;
    delete newData.tensionIds;
    newData.authorUserId = this.contextUser?.id;
    const driver = await super.create(newData);
    if (tensionIds) {
      await this.linkDriverAndTensions(tensionIds);
    }
    return driver;
  }

  public async edit(newData: Partial<IDriver>): Promise<IDriver> {
    const { tensionIds } = newData;
    delete newData.tensionIds;
    delete newData.authorUserId;
    delete newData.domainId;

    if (tensionIds) {
      await this.changeTensionLinks(tensionIds);
    }
    return super.edit(newData);
  }

  public async checkEntityAccess(newData: IDriver): Promise<boolean> {
    if (this.domainHierarchy && newData.domainId) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      if (!domainIds.includes(newData.domainId as string)) {
        throw new Error('Access denied');
      }
    }
    return !!newData;  
  }

  public async checkCreateEditData(newData: Partial<IDriver>, isCreate?: boolean): Promise<boolean> {
    const data = isCreate ? undefined : await this.data;
    const domainId = isCreate ? newData.domainId : data?.domainId;
    if (newData.tensionIds) {
      const tensionRepositories = new TensionRepositories(this.contextUser);
      await tensionRepositories.checkEntitiesOfOrganization(newData.tensionIds, domainId as string);
    }
    
    return true;
  }

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<IDriver[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      entities.forEach(entity => {
        if (!domainHierarchy.checkDomainInOrganization(entity.domainId as string, organizationId)) {
          throw new Error('Access denied');
        }
      });
    }

    return entities;
  }

  public async getAuthorUser(): Promise<IUser | null> {
    const data = await this.data;
    return new UserRepositories(this.contextUser, data?.authorUserId).getData();
  }

  public async getDomain(): Promise<IDomain | null> {
    const data = await this.data;
    return new DomainRepositories(this.contextUser, data?.domainId).getData();
  }

  public async getTensions(): Promise<ITension[]> {
    const tensionDriver = await dbClient('TensionDriver')
      .where('driverId', this.id)
      .select('tensionId');
    const tensionIds = tensionDriver.map(tension => tension.tensionId);
    const tensionRepositories = new TensionRepositories(this.contextUser);
    return tensionRepositories.getByIds(tensionIds);
  }

  public async getOfOrganization(domainId: string): Promise<IDriver[]> {
    const ids = (await this.domainHierarchy)?.getOrganizationFromDomain(domainId)?.subDomainsFlat.map((domain) => domain.id);
    return this.getByQuery((query) => query.whereIn('domainId', ids || []));
  }

}