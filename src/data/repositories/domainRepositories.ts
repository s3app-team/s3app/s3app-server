import { DomainType, IDomain } from '../../types/IDomain';
import dbClient from '../../utils/knex';
import { NotFoundError } from '../erros';
import { IEntityId } from '../../types/IEntityId';
import BaseRepository from './baseRepository';
import { IUser } from '../../types/IUser';
import { UserInputError } from 'apollo-server-express';
import { IStage } from '../../types/IStage';
import { IIssue } from '../../types/IIssue';
import { ISprint } from '../../types/ISprint';
import { ITension } from '../../types/ITension';
import { StageRepositories } from './stageRepositories';
import { IssueFilter, IssueRepositories, IssueSort } from './issueRepositories';
import { SprintRepositories } from './sprintRepositories';
import { TensionRepositories } from './tensionRepositories';
import { UserRepositories } from './userRepositories';
import { Knex } from 'knex';
import { OrganizationRepositories } from './organizationRepositories';
import { IDriver } from '../../types/IDriver';
import { IProposal } from '../../types/IProposal';
import { ProposalRepositories } from './proposalRepositories';
import { DriverRepositories } from './driverRepositories';
import config from '../../libs/config';
import { IEvent } from '../../types/IEvent';
import { EventRepositories } from './eventRepositories';
import { IThought } from '../../types/IThought';
import { ThoughtRepositories } from './thoughtRepositories';
import { IssueTypeRepositories } from './issueTypeRepositories';
import { IIssueType } from '../../types/IIssueType';
export class DomainRepositories extends BaseRepository<IDomain> {
  getTable(): string {
    return 'Domain';
  }

  public async getParticipants() {
    const domain = await this.getData();
    if (!domain) {
      throw new NotFoundError('Domain not found');
    }
    const participantIds = await this.getParticipantIds();
    let participants:IUser[] = [];
    if (participantIds?.length) {
      participants = await new UserRepositories(this.contextUser).getByIds(participantIds);
    }
    return participants;
  }

  public async create(data: IDomain) {
    const { subDomains, participants } = data;
    delete data.subDomains;
    delete data.participants;
    const domain = await super.create(data);
    if (!data.parentDomainId) {
      data.domainType = DomainType.organization;
    }
    if (data.domainType === 'organization') {
      if (config.oneOrganization) {
        const organizations = await this.getByFields({ domainType: DomainType.organization });
        if (organizations.length) {
          throw new UserInputError('Cannot create another organization');
        }
      }
      if (data.parentDomainId) {
        throw new UserInputError('Organization cannot have parent domain');
      }
      const organizationRepository = new OrganizationRepositories(this.contextUser, domain.id);
      await organizationRepository.createOrganization();
    }

    if ((data.domainType === 'circle' || data.domainType === 'role') && !data.parentDomainId) {
      throw new UserInputError('Circle and role must have parent domain');
    }

    if (data.domainType === 'service') {
      throw new UserInputError('Not implemented');
    }

    if (data.domainType === 'role') {
      const parentDomain = await new DomainRepositories(this.contextUser, data.parentDomainId).getData();
      if (parentDomain.domainType === 'role') {
        throw new UserInputError('Role cannot have parent domain of type role');
      }
    }

    if (domain.id) {
      await new StageRepositories(this.contextUser).create({
        domainId: domain.id,
        name: 'TODO',
      });
      await new StageRepositories(this.contextUser).create({
        domainId: domain.id,
        name: 'В работе',
      });
      await new StageRepositories(this.contextUser).create({
        domainId: domain.id,
        name: 'Проверяется',
      });
      await new StageRepositories(this.contextUser).create({
        domainId: domain.id,
        name: 'Готово',
      });
    }

    if (subDomains && subDomains.length && domain?.id) {
      const subDomainsExists = await this.checkExistDomains(subDomains);
      if (!subDomainsExists) {
        throw new NotFoundError('Some subdomains do not exist');
      }
      this.changeParentDomain(subDomains, this.id);
    }
    if (participants?.length && domain.id) {
      this.addParticipants(participants);
    }

    //TODO: добавить логику при добавлении circle из прошлых версий
    return domain;
  }

  

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<IDomain[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      entities.forEach(entity => {
        if (!domainHierarchy.checkDomainInOrganization(entity.id as string, organizationId)) {
          throw new Error('Access denied');
        }
      });
    }

    return entities;
  }

  public async checkCreateEditData(newData: Partial<IDomain>, isCreate?: boolean): Promise<boolean> {
    const domainId = isCreate ? newData.id : this.id;
    if (newData.participants) {
      const userRepositories = new UserRepositories(this.contextUser);
      await userRepositories.checkEntitiesOfOrganization(newData.participants, domainId as string);
    }
    if (newData.mainDriverId) {
      const tensionRepositories = new TensionRepositories(this.contextUser);
      await tensionRepositories.checkEntitiesOfOrganization([newData.mainDriverId], domainId as string);
    }
    if (newData.parentDomainId) {
      const domains = await (await this.domainHierarchy)?.getUserDomainIds();
      if (!domains?.includes(newData.parentDomainId)) {
        throw new UserInputError('Parent domain not found');
      }
    }

    return true;
  }

  async filterEntities(knex: Knex.QueryBuilder) {
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      return knex.whereIn('id', domainIds);
    }
    return knex;
  }

  public async addParticipants(userIds: string[]) {
    const domain = await this.getData();

    if (!domain) {
      throw new NotFoundError('Domain not found');
    }
    const allUserExist = await new UserRepositories(this.contextUser).checkUsersExist(userIds);
    if (!allUserExist) {
      throw new NotFoundError('Some users do not exist');
    }
    await new UserRepositories(this.contextUser).checkEntitiesOfOrganization(userIds, this.id as string);
    let newParticipantIds: string[] = userIds;
    const domainParticipantIds = await this.getParticipantIds();
    if (domainParticipantIds?.length) {
      newParticipantIds = userIds.filter(
        (userId) => !domainParticipantIds.includes(userId),
      );
      if (newParticipantIds.length === 0) {
        throw new UserInputError('All users already in domain');
      }
    }
    const updateDomain = await this.addUsers(
      newParticipantIds,
    );

    return updateDomain;
  }



  public async removeParticipants(userIds: string[]) {
    const domain = await this.getData();

    await (await this.domainHierarchy)?.checkAdmin(this.id as string);

    if (!domain) {
      throw new NotFoundError('Domain not found');
    }
    const allUserExist = await new UserRepositories(this.contextUser).checkUsersExist(userIds);
    if (!allUserExist) {
      throw new NotFoundError('Some users do not exist');
    }
    const updateDomain = await this.removeUsers(
      userIds,
    );
    return updateDomain;
  }

  public async addSubdomains(subDomainIds: string[]) {
    const parentDomain = await this.getData();
    if (!parentDomain) {
      throw new NotFoundError('Parent domain not found');
    }
    const isSubdomainAndParentSame = subDomainIds.includes(this.id as string);
    if (isSubdomainAndParentSame) {
      throw new UserInputError('Subdomain and parent domain are the same');
    }
    const subDomainsExists = await this.checkExistDomains(subDomainIds);
    if (!subDomainsExists) {
      throw new NotFoundError('Some subdomains do not exist');
    }
    await this.checkEntitiesOfOrganization(subDomainIds, this.id as string);
    return this.changeParentDomain(subDomainIds, this.id);
  }

  public async removeSubdomains(
    subDomainIds: string[],
  ) {
    const parentDomain = await this.getData();
    if (!parentDomain) {
      throw new NotFoundError('Parent domain not found');
    }
    const subDomainsExists = await this.checkExistDomains(subDomainIds);
    if (!subDomainsExists) {
      throw new NotFoundError('Some subdomains do not exist');
    }
    return this.changeParentDomain(subDomainIds);
  }

  public async edit(updateDomain: Partial<IDomain>): Promise<IDomain> {
    const { subDomains } = updateDomain;
    delete updateDomain.subDomains;
    delete updateDomain.domainType;
    delete updateDomain.parentDomainId;
    if (subDomains && subDomains.length && this.id) {
      const subDomainsExists = await this.checkExistDomains(subDomains);
      if (!subDomainsExists) {
        throw new NotFoundError('Some subdomains do not exist');
      }
      this.changeParentDomain(subDomains);
    }
    return super.edit(updateDomain);
  }

  public async delete(): Promise<boolean> {
    await (await this.domainHierarchy)?.checkAdmin(this.id as string);
    return super.delete();
  }

  public async changeParentDomain(subDomainIds: string[], parentDomainId?: string): Promise<IEntityId> {
    await this.getData();
    await this.checkEntitiesOfOrganization(subDomainIds, this.id as string);
    await dbClient('Domain')
      .whereIn('id', subDomainIds)
      .update({
        parentDomainId,
      });
    return { id: this.id };
  }

  public async addUsers(userIds: string[]): Promise<IEntityId[]> {
    await this.getData();
    await new UserRepositories(this.contextUser).checkEntitiesOfOrganization(userIds, this.id as string);
    const insertData = userIds.map(userId => ({
      domainId: this.id,
      userId: userId,
    }));
    const insertedDomainUserLinks: IEntityId[] = await dbClient('UserDomainLink')
      .insert(insertData)
      .returning('id');

    return insertedDomainUserLinks;
  }

  public async removeUsers(userIds: string[]): Promise<IEntityId | never> {
    await this.getData();
    await dbClient('UserDomainLink')
      .where('domainId', this.id)
      .whereIn('userId', userIds)
      .update({
        isDeleted: true,
        deletedAt: new Date(),
      });
    return { id: this.id };
  }

  public async getDomainParticipantIds() {
    const participantIds = await dbClient('UserDomainLink')
      .where('domainId', this.id)
      .where('isDeleted', false)
      .orderBy('createdAt', 'desc')
      .select('userId');
    return participantIds;
  }

  public async getParticipantIds(): Promise<string[]> {
    const participantIds = await dbClient('UserDomainLink')
      .where('domainId', this.id)
      .where('isDeleted', false)
      .orderBy('createdAt', 'desc')
      .pluck('userId');
    return participantIds;
  }

  public async getByParentDomain(parentDomainId: string): Promise<IDomain[]> {
    return this.getByFields({ parentDomainId });
  }

  public async checkExistDomains(ids: string[]): Promise<boolean> {
    const existingDomains = await dbClient('Domain')
      .whereIn('id', ids)
      .select('id');

    return existingDomains.length === ids.length;
  }

  public async getStages(): Promise<IStage[]> {
    return new StageRepositories(this.contextUser, this.id).getByDomainId(this.id as string);
  }

  public async getIssueTypes(): Promise<IIssueType[]> {
    return new IssueTypeRepositories(this.contextUser, this.id).getByDomainId(this.id as string);
  }

  public async getIssues(withSubDomains: boolean = false, 
    page?: number, perPage?: number, filters?: IssueFilter, sort?: IssueSort): Promise<IIssue[]> {
    return new IssueRepositories(this.contextUser, this.id).getByDomain(this.id as string, withSubDomains,
      page, perPage, filters, sort,
    );
  }

  public async getIssuesCount(withSubDomains: boolean = false, 
    filters?: IssueFilter): Promise<number> {
    return new IssueRepositories(this.contextUser, this.id).getByDomainCount(this.id as string, withSubDomains,
      filters,
    );
  }

  public async getSprints(withSubDomains: boolean = false): Promise<ISprint[]> {
    return new SprintRepositories(this.contextUser, this.id).getByDomain(this.id as string, withSubDomains);
  }

  public async getEvents(withSubDomains: boolean = false): Promise<IEvent[]> {
    return new EventRepositories(this.contextUser, this.id).getByDomain(this.id as string, withSubDomains);
  }

  public async getThoughts(withSubDomains: boolean = false): Promise<IThought[]> {
    return new ThoughtRepositories(this.contextUser, this.id).getByDomain(this.id as string, withSubDomains);
  }

  public async getTensions(withSubDomains: boolean = false): Promise<ITension[]> {
    return new TensionRepositories(this.contextUser, this.id).getByDomain(this.id as string, withSubDomains);
  }

  public async getDrivers(withSubDomains: boolean = false): Promise<IDriver[]> {
    return new DriverRepositories(this.contextUser, this.id).getByDomain(this.id as string, withSubDomains);
  }

  public async getProposals(withSubDomains: boolean = false): Promise<IProposal[]> {
    return new ProposalRepositories(this.contextUser, this.id).getByDomain(this.id as string, withSubDomains);
  }

  public async getMainDriver(): Promise<IDriver | null> {
    const data = await this.data;
    if (data?.mainDriverId) {
      return new DriverRepositories(this.contextUser, data.mainDriverId).getData();
    }
    return null;
  }

  public async getOfOrganization(): Promise<IDomain[]> {
    const ids = (await this.domainHierarchy)?.getOrganizationFromDomain(this.id as string)?.subDomainsFlat.map((domain) => domain.id);
    return this.getByIds(ids || []);
  }   

  orderByDirection(): string {
    return 'asc';
  }
}
