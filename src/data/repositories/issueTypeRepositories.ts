import dbClient from '../../utils/knex';
import { NotFoundError } from '../erros';
import BaseRepository from './baseRepository';
import { StageRepositories } from './stageRepositories';
import { IIssueType } from '../../types/IIssueType';
import { IStage } from '../../types/IStage';
import { IDomain } from '../../types/IDomain';
import { DomainRepositories } from './domainRepositories';
import { IEntityId } from '../../types/IEntityId';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type ThrowException<T = any> = never;


export class IssueTypeRepositories extends BaseRepository<IIssueType> {
  getTable(): string {
    return 'IssueType';
  }

  public async getByDomainId(domainId: string) {
    return this.getByFields({ domainId });
  }

  public async linkIssueTypeAndStages(stageIds: string[]) {
    const stages = await new StageRepositories(this.contextUser).getByIds(stageIds);
    if (!stages || stages.length !== stageIds.length) {
      throw new NotFoundError('Stages not found');
    }
    const issueType = await this.getData();
    if (!issueType) {
      throw new NotFoundError('IssueType not found');
    }
    const insertData = stageIds.map(stageId => ({
      issueTypeId: this.id,
      stageId,
    }));
    if (insertData.length) {
      const insertedStageLinks = await dbClient('IssueTypeStage')
        .insert(insertData)
        .returning('id');
      return insertedStageLinks;
    }
    return [];
  }

  public async unlinkIssueTypeAndStages(stageIds: string[] | string[]): Promise<boolean> {
    await this.getData();
    if (stageIds.length) {
      await dbClient('IssueTypeStage')
        .delete()
        .where('issueTypeId', this.id)
        .whereIn('stageId', stageIds);
      return true;
    }
    return true;
  }

  public async changeStageLinks(stageIds: string[]): Promise<IEntityId> {
    const data = await this.data;
    await this.checkEntitiesOfOrganization(stageIds, data?.domainId as string);
    const currentStages = await this.getStages();
    const currentStageLinksIds = currentStages.map((stage) => stage.id);
    const toAdd = stageIds.filter((issue) => !currentStageLinksIds.includes(issue));
    const toDelete = currentStageLinksIds.filter((stage) => !stageIds.includes(stage));
    await this.linkIssueTypeAndStages(toAdd);
    await this.unlinkIssueTypeAndStages(toDelete);
    return { id: this.id };
  }

  public async create(newData: Partial<IIssueType>): Promise<IIssueType> {
    const { stageIds } = newData;
    delete newData.stageIds;
    const issueType = await super.create(newData);
    if (stageIds) {
      await this.linkIssueTypeAndStages(stageIds);
    }
    return issueType;
  }

  public async edit(newData: Partial<IIssueType>): Promise<IIssueType> {
    const { stageIds } = newData;
    delete newData.stageIds;
    delete newData.authorUserId;
    delete newData.domainId;

    if (stageIds) {
      await this.changeStageLinks(stageIds);
    }
    return super.edit(newData);
  }

  public async checkEntityAccess(newData: IIssueType): Promise<boolean> {
    if (this.domainHierarchy && newData.domainId) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      if (!domainIds.includes(newData.domainId as string)) {
        throw new Error('Access denied');
      }
    }
    if (newData.stageIds) {
      const data = await this.getData();
      const stages = await new StageRepositories(this.contextUser).getByIds(newData.stageIds);
      stages.forEach((stage) => {
        if (stage.domainId !== data?.domainId) {
          throw new Error('Stages not found');
        }
      });
    }  
    return !!newData;  
  }

  public async checkCreateEditData(newData: Partial<IIssueType>, isCreate?: boolean): Promise<boolean> {
    const data = isCreate ? undefined : await this.data;
    const domainId = isCreate ? newData.domainId : data?.domainId;
    if (newData.stageIds) {
      const stageRepositories = new StageRepositories(this.contextUser);
      await stageRepositories.checkEntitiesOfOrganization(newData.stageIds, domainId as string);
    }
    
    return true;
  }

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<IIssueType[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      entities.forEach(entity => {
        if (!domainHierarchy.checkDomainInOrganization(entity.domainId as string, organizationId)) {
          throw new Error('Access denied');
        }
      });
    }

    return entities;
  }

  public async getDomain(): Promise<IDomain | null> {
    const data = await this.data;
    return new DomainRepositories(this.contextUser, data?.domainId).getData();
  }

  public async getStages(): Promise<IStage[]> {
    const IssueTypeStage = await dbClient('IssueTypeStage')
      .where('issueTypeId', this.id)
      .select('stageId');
    const stageIds = IssueTypeStage.map(stage => stage.stageId);
    const stageRepositories = new StageRepositories(this.contextUser);
    return stageRepositories.getByIds(stageIds);
  }

  public async getOfOrganization(domainId: string): Promise<IIssueType[]> {
    const ids = (await this.domainHierarchy)?.getOrganizationFromDomain(domainId)?.subDomainsFlat.map((domain) => domain.id);
    return this.getByQuery((query) => query.whereIn('domainId', ids || []));
  }

}