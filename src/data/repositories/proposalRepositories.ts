import dbClient from '../../utils/knex';
import { NotFoundError } from '../erros';
import { IProposal } from '../../types/IProposal';
import BaseRepository from './baseRepository';
import { IUser } from '../../types/IUser';
import { UserRepositories } from './userRepositories';
import { DriverRepositories } from './driverRepositories';
import { IDriver } from '../../types/IDriver';
import { IDomain } from '../../types/IDomain';
import { DomainRepositories } from './domainRepositories';
import { IEntityId } from '../../types/IEntityId';
import { CommentRepositories } from './commentRepositories';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type ThrowException<T = any> = never;


export class ProposalRepositories extends BaseRepository<IProposal> {
  public async getComments() {
    return new CommentRepositories(this.contextUser).getByProposal(
      this.id as string,
    );
  }

  getTable(): string {
    return 'Proposal';
  }

  public async getByDomain(domainId: string, withSubDomains: boolean) {
    if (withSubDomains) {
      const subdomains = (await this.domainHierarchy)?.findSubDomains(
        domainId,
      );
      return this.getByQuery(_knex => 
        _knex.whereIn(
          'domainId',
          subdomains?.map((domain) => domain.id) || [],
        ));
    }
    return this.getByFields({ domainId });
  }

  public async getDomain(): Promise<IDomain | null> {
    const data = await this.data;
    return new DomainRepositories(this.contextUser, data?.domainId).getData();
  }

  public async linkProposalAndDrivers(driversIds: string[]) {
    const drivers = await new DriverRepositories(this.contextUser).getByIds(driversIds);
    if (!drivers || drivers.length !== driversIds.length) {
      throw new NotFoundError('Drivers not found');
    }
    const proposal = await this.getData();
    if (!proposal) {
      throw new NotFoundError('Proposal not found');
    }
    const insertData = driversIds.map(driverId => ({
      proposalId: this.id,
      driverId,
    }));
    if (insertData.length) {
      const insertedDriverLinks = await dbClient('DriverProposal')
        .insert(insertData)
        .returning('id');
      return insertedDriverLinks;
    }
    return [];
  }

  public async unlinkProposalAndDrivers(driverIds: string[] | string[]): Promise<boolean> {
    await this.getData();
    if (driverIds.length) {
      await dbClient('DriverProposal')
        .delete()
        .where('proposalId', this.id)
        .whereIn('driverId', driverIds);
      return true;
    }
    return true;
  }

  public async changeDriverLinks(driversIds: string[]): Promise<IEntityId> {
    const data = await this.data;
    await this.checkEntitiesOfOrganization(driversIds, data?.domainId as string);
    const currentDrivers = await this.getDrivers();
    const currentDriverLinksIds = currentDrivers.map((driver) => driver.id);
    const toAdd = driversIds.filter((driver) => !currentDriverLinksIds.includes(driver));
    const toDelete = currentDriverLinksIds.filter((driver) => !driversIds.includes(driver));
    await this.linkProposalAndDrivers(toAdd);
    await this.unlinkProposalAndDrivers(toDelete);
    return { id: this.id };
  }

  public async create(newData: Partial<IProposal>): Promise<IProposal> {
    const { driverIds } = newData;
    delete newData.driverIds;
    newData.authorUserId = this.contextUser?.id;
    const driver = await super.create(newData);
    if (driverIds) {
      await this.linkProposalAndDrivers(driverIds);
    }
    return driver;
  }

  public async edit(newData: Partial<IProposal>): Promise<IProposal> {
    const { driverIds } = newData;
    delete newData.driverIds;
    delete newData.authorUserId;
    delete newData.domainId;

    if (driverIds) {
      await this.changeDriverLinks(driverIds);
    }
    return super.edit(newData);
  }

  public async checkEntityAccess(newData: IProposal): Promise<boolean> {
    if (this.domainHierarchy && newData.domainId) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      if (!domainIds.includes(newData.domainId as string)) {
        throw new Error('Access denied');
      }
    }
    return !!newData;  
  }

  public async checkCreateEditData(newData: Partial<IProposal>, isCreate?: boolean): Promise<boolean> {
    const data = isCreate ? undefined : await this.data;
    const domainId = isCreate ? newData.domainId : data?.domainId;
    if (newData.driverIds) {
      const driverRepositories = new DriverRepositories(this.contextUser);
      await driverRepositories.checkEntitiesOfOrganization(newData.driverIds, domainId as string);
    }
    
    return true;
  }

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<IProposal[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      entities.forEach(entity => {
        if (!domainHierarchy.checkDomainInOrganization(entity.domainId as string, organizationId)) {
          throw new Error('Access denied');
        }
      });
    }

    return entities;
  }

  public async getAuthorUser(): Promise<IUser | null> {
    const data = await this.data;
    return new UserRepositories(this.contextUser, data?.authorUserId).getData();
  }

  public async getDrivers(): Promise<IDriver[]> {
    const driverProposal = await dbClient('DriverProposal')
      .where('proposalId', this.id)
      .select('driverId');
    const driverIds = driverProposal.map(dp => dp.driverId);
    const driverRepositories = new DriverRepositories(this.contextUser);
    return driverRepositories.getByIds(driverIds);
  }

  public async getOfOrganization(domainId: string): Promise<IProposal[]> {
    const ids = (await this.domainHierarchy)?.getOrganizationFromDomain(domainId)?.subDomainsFlat.map((domain) => domain.id);
    return this.getByQuery((query) => query.whereIn('domainId', ids || []));
  }

}