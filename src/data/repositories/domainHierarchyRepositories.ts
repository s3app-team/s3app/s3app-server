import { IDomain } from '../../types/IDomain';
import dbClient from '../../utils/knex';
import { IUser } from '../../types/IUser';
import BaseInternalRepository from './baseInternalRepository';
import { AccessRole } from '../../types/IUserOrganizationLink';

interface DomainNode {
  domain: IDomain,
  subDomains: DomainNode[]
  subDomainsFlat: IDomain[]
}

class DomainHierarchyRepositories extends BaseInternalRepository<IDomain> {

  getTable(): string {
    return 'Domain';
  }

  isInternal = true;

  domainTree: DomainNode[] = [];

  static async Create(contextUser: IUser): Promise<DomainHierarchyRepositories> {
    const domainHierarchy = new DomainHierarchyRepositories(contextUser);
    await domainHierarchy.init();
    return domainHierarchy;
  }

  async init() {
    const domains:IDomain[] = await this.getAll();
    domains.forEach(domain => {
      if (!domain.parentDomainId) {
        this.domainTree.push({ domain, subDomains: [], subDomainsFlat: [domain] });
        this.addNode(this.domainTree[this.domainTree.length - 1], domains, 
          this.domainTree[this.domainTree.length - 1].subDomainsFlat);
      }
    });
  }

  addNode(parentNode: DomainNode, domains: IDomain[], subDomainsFlat: IDomain[]) {
    domains.forEach(domain => {
      if (domain.parentDomainId === parentNode.domain.id) {
        parentNode.subDomains.push({ domain, subDomains: [], subDomainsFlat: [] });
        subDomainsFlat.push(domain);
        this.addNode(parentNode.subDomains[parentNode.subDomains.length - 1], domains, subDomainsFlat);
      }
    });
  }

  async getUserOrganizations(userId: string): Promise<DomainNode[]> {
    const organizationsIds = (await dbClient('UserOrganizationLink')
      .select('*')
      .where('userId', userId)
      .orderBy('createdAt', 'desc'))
      .map(organization => organization.organizationId);
    const organizations: DomainNode[] = [];
    this.domainTree.forEach(domainNode => {
      if (organizationsIds.includes(domainNode.domain.id || '')) {
        organizations.push(domainNode);
      }
    });
    return organizations;
  }

  async getUserDomainIds(): Promise<string[]> {
    const organizations = await this.getUserOrganizations(this.contextUser?.id as string);
    return organizations.flatMap(organization => organization.subDomainsFlat).map(domain => domain.id || '');
  }

  checkDomainInOrganization(domainId: string, organizationId: string): boolean {
    const organization = this.domainTree.find(domainNode => domainNode.domain.id === organizationId);
    return organization?.subDomainsFlat.some(domain => domain.id === domainId) || false;
  }

  async checkUserDomain(domainId: string): Promise<boolean> {
    const userOrganizations = await this.getUserOrganizations(this.contextUser?.id as string);
    return userOrganizations.some(organization => organization.subDomainsFlat.some(domain => domain.id === domainId));
  }

  async getUserOrganizationsIds(userId: string): Promise<string[]> {
    return (await this.getUserOrganizations(userId))
      .map(userOrganization => userOrganization.domain.id);
  }

  getOrganizationFromDomain(domainId: string): DomainNode | undefined {
    return this.domainTree.find(domainNode => domainNode.subDomainsFlat.some(domain => domain.id === domainId));
  }

  async checkAdmin(domainId:string): Promise<boolean> {
    if (this.contextUser) {
      if ((await this.getOrganizationAccessRole(domainId)) !== AccessRole.admin) {
        throw new Error('Access denied');
      }
    }
    return true;
  }

  public async getOrganizationAccessRole(domainId: string): Promise<AccessRole> {
    const organization = this.getOrganizationFromDomain(domainId);
    const userOrganizationLink = await dbClient('UserOrganizationLink')
      .select('accessRole')
      .where('organizationId', organization?.domain.id)
      .where('userId', this.contextUser?.id)
      .first();
    return userOrganizationLink.accessRole;
  }

  findSubDomains(subDomainId: string): IDomain[] {
    const organization = this.getOrganizationFromDomain(subDomainId);
    const foundDomains: IDomain[] = [];
    this.findSubDomainsRecursive(organization as DomainNode, subDomainId, foundDomains);
    return foundDomains;
  }

  findSubDomainsRecursive(parentNode: DomainNode, subDomainId: string, foundDomains: IDomain[] = [], isFound = false): void {
    if (parentNode.domain.id === subDomainId || isFound) {
      isFound = true;
      foundDomains.push(parentNode.domain);
    }
    parentNode.subDomains.forEach(domainNode => {
      this.findSubDomainsRecursive(domainNode, subDomainId, foundDomains, isFound);
    });
  }

  // async checkUserEvent(eventId: string): Promise<boolean> {
  //   const event = await new EventRepositories(undefined, eventId, true).getData();
  //   return this.checkUserDomain(event?.domainId as string);
  // }

  // async checkUserSprint(sprintId: string): Promise<boolean> {
  //   const sprint = await new SprintRepositories(undefined, sprintId, true).getData();
  //   return this.checkUserDomain(sprint?.domainId || '');
  // }

  // async checkUserProposal(proposalId: string): Promise<boolean> {
  //   const proposal = await new ProposalRepositories(undefined, proposalId, true).getData();
  //   return this.checkUserDomain(proposal?.domainId || '');
  // }

  // async checkUserTension(tensionId: string): Promise<boolean> {
  //   const tension = await new TensionRepositories(undefined, tensionId, true).getData();
  //   return this.checkUserDomain(tension?.domainId || '');
  // }

  // async checkUserIssue(issueId: string): Promise<boolean> {
  //   const issue = await new IssueRepositories(undefined, issueId, true).getData();
  //   return this.checkUserDomain(issue?.domainId || '');
  // }

  // async checkUserComment(commentId: string): Promise<boolean> {
  //   const comment = await new IssueRepositories(undefined, commentId, true).getData();
  //   return this.checkUserDomain(comment?.domainId || '');
  // }

  // async checkUserStage(stageId: string): Promise<boolean> {
  //   const stage = await new StageRepositories(undefined, stageId, true).getData();
  //   return this.checkUserDomain(stage?.domainId || '');
  // }
}

export default DomainHierarchyRepositories;