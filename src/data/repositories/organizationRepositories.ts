import { IEntityId } from '../../types/IEntityId';
import { AccessRole, IUserOrganizationLink } from '../../types/IUserOrganizationLink';
import dbClient from '../../utils/knex';
import { UserRepositories } from './userRepositories';
import { IUser } from '../../types/IUser';
import DomainHierarchyRepositories from './domainHierarchyRepositories';
import BaseRepository from './baseRepository';
import { IDomain } from '../../types/IDomain';
import { InviteRepositories } from './inviteRepositories';

export class OrganizationRepositories extends BaseRepository<IDomain> {
  getTable() {
    return 'Domain';
  }

  needContextUser(): boolean | undefined {
    return undefined;
  }

  public static async CreateFromDomain(contextUser: IUser, id: string, isInternal = false) {
    const domainHierarchy = await DomainHierarchyRepositories.Create(contextUser);
    return new OrganizationRepositories(contextUser, domainHierarchy.getOrganizationFromDomain(id)?.domain.id, isInternal);
  }

  public static async CreateFromInvite(contextUser: IUser, inviteId: string) {
    const invite = await dbClient('UserOrganizationInvite')
      .select('*')
      .where('id', inviteId)
      .first();
    if (!invite) {
      throw new Error('Invite not found');
    }
    return new OrganizationRepositories(contextUser, invite.organizationId);
  }

  public async checkAdmin():Promise<boolean> {
    return (await this.domainHierarchy)?.checkAdmin(this.id as string) || false;
  }

  public async addUsersToOrganization(input: {
    userId: string,
    accessRole: AccessRole,
  }[]): Promise<IEntityId[]> {
    const insertData = input.map(user => ({
      organizationId: this.id,
      userId: user.userId,
      accessRole: user.accessRole,
    }));
    const insertedOrganizationUserLinks: IEntityId[] = await dbClient('UserOrganizationLink')
      .insert(insertData)
      .returning('userId')
      .onConflict().ignore();
    
    return insertedOrganizationUserLinks;
  }

  public async createOrganization() {
    await dbClient('UserOrganizationLink')
      .insert({
        organizationId: this.id,
        userId: this.contextUser?.id,
        accessRole: AccessRole.admin,
      })
      .returning('userId');
  }

  public async getAccessRole(): Promise<AccessRole> {
    await this.getData();
    const userOrganizationLink = await dbClient('UserOrganizationLink')
      .select('accessRole')
      .where('organizationId', this.id)
      .where('userId', this.contextUser?.id)
      .first();
    return userOrganizationLink.accessRole;
  }
    
  public async removeUsersFromOrganization(userIds: string[]): Promise<IEntityId | never> {
    await this.checkAdmin();
    
    const users:IUserOrganizationLink[] = await dbClient('UserOrganizationLink')
      .where('organizationId', this.id);

    if (users.filter(user => user.accessRole === AccessRole.admin).length === 1) {
      throw new Error('At least one admin should be in the organization');
    }

    await dbClient('UserOrganizationLink')
      .where('organizationId', this.id)
      .whereIn('userId', userIds)
      .del();
    return { id: this.id };
  }
    
  public async changeUserAccessRole(userId: string, accessRole: AccessRole): Promise<IEntityId> {
    if (this.contextUser?.id === userId) {
      throw new Error('Access denied');
    }
    await this.checkAdmin();

    const users:IUserOrganizationLink[] = await dbClient('UserOrganizationLink')
      .where('organizationId', this.id);

    if (accessRole === AccessRole.user && users.filter(user => user.accessRole === AccessRole.admin).length === 1) {
      throw new Error('At least one admin should be in the organization');
    }

    await dbClient('UserOrganizationLink')
      .where('organizationId', this.id)
      .where('userId', userId)
      .update({
        accessRole,
      });
    return { id: this.id };
  }

  public async getUserOrganizationLinks(): Promise<IUserOrganizationLink[]> {
    const userIds = await dbClient('UserOrganizationLink')
      .select('*')
      .where('organizationId', this.id)
      .orderBy('createdAt', 'asc')
      .orderBy('userId', 'asc');
    return userIds;
  }

  public async getOranizationUsers(): Promise<(IUserOrganizationLink & { user: IUser })[]> {
    await this.getData();
    const result: (IUserOrganizationLink & { user: IUser })[] = [];
    const organizationUsers = await this.getUserOrganizationLinks();
    const users = await new UserRepositories(this.contextUser).getByIds(organizationUsers.map(user => user.userId));
    organizationUsers.forEach((user) => {
      result.push({
        ...user,
        user: users.find((u) => u.id === user.userId) as IUser,
      });
    });
    return result;
  }

  public async sendInvite(name: string, email: string, license: string): Promise<boolean> {
    const user = await new UserRepositories().getByEmail(email);
    const data = await this.data;
    if (user) {
      this.addUsersToOrganization([{ userId: user.id, accessRole: AccessRole.user }]);
      return true;
    }
    await new InviteRepositories(this.contextUser).create({
      name,
      email,
      organizationId: data?.id,
      license,
    });
    return false;
  }
}