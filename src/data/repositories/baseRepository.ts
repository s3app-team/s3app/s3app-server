import { Knex } from 'knex';
import { IEntity } from '../../types/IEntity';
import { IUser } from '../../types/IUser';
import BaseInternalRepository from './baseInternalRepository';
import DomainHierarchyRepositories from './domainHierarchyRepositories';

abstract class BaseRepository<T extends IEntity> extends BaseInternalRepository<T> {

  domainHierarchy?: Promise<DomainHierarchyRepositories>;

  constructor(contextUser: IUser | undefined = undefined, id: string | T | undefined = undefined, isInternal = false) {
    super(contextUser, id, isInternal);

    if (!this.isInternal && contextUser) {
      this.domainHierarchy = DomainHierarchyRepositories.Create(contextUser);
    }
  }

  async filterEntities(knex: Knex.QueryBuilder) {
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      return knex.whereIn('domainId', domainIds);
    }
    return knex;
  }
}

export default BaseRepository;