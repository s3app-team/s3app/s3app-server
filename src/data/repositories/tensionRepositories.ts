import { ITension } from '../../types/ITension';
import BaseRepository from './baseRepository';
import { IUser } from '../../types/IUser';
import { UserRepositories } from './userRepositories';
import { IDomain } from '../../types/IDomain';
import { DomainRepositories } from './domainRepositories';
import { CommentRepositories } from './commentRepositories';
export class TensionRepositories extends BaseRepository<ITension> {
  getTable(): string {
    return 'Tension';
  }

  public async getByDomain(domainId: string, withSubDomains: boolean) {
    if (withSubDomains && (await this.domainHierarchy)) {
      const subdomains = (await this.domainHierarchy)?.findSubDomains(
        domainId,
      );
      return this.getByQuery(_knex => 
        _knex.whereIn(
          'domainId',
          subdomains?.map((domain) => domain.id) || [],
        ));
    }
    return this.getByFields({ domainId });
  }

  public async getDomain(): Promise<IDomain | null> {
    const data = await this.data;
    return new DomainRepositories(
      this.contextUser,
      data?.domainId,
    ).getData();
  }

  async getAuthorUser(): Promise<IUser> {
    const data = await this.data;
    return new UserRepositories(
      this.contextUser,
      data?.authorUserId,
    ).getData();
  }

  public async create(newData: Partial<ITension>): Promise<ITension> {
    newData.authorUserId = this.contextUser?.id;
    return super.create(newData);
  }

  public async checkEntityAccess(entity: ITension): Promise<boolean> {
    if (this.domainHierarchy && entity.domainId) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      if (!domainIds.includes(entity.domainId as string)) {
        throw new Error('Access denied');
      }
    }
    return !!entity;
  }

  public async checkEntitiesOfOrganization(
    ids: string[],
    domainId: string,
  ): Promise<ITension[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId =
                domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      entities.forEach((entity) => {
        if (
          !domainHierarchy.checkDomainInOrganization(
            entity.domainId as string,
            organizationId,
          )
        ) {
          throw new Error('Access denied');
        }
      });
    }

    return entities;
  }

  public async getOfOrganization(domainId: string): Promise<ITension[]> {
    const ids = (await this.domainHierarchy)
      ?.getOrganizationFromDomain(domainId)
      ?.subDomainsFlat.map((domain) => domain.id);
    return this.getByQuery((query) => query.whereIn('domainId', ids || []));
  }

  public async getComments() {
    return new CommentRepositories(this.contextUser).getByTension(
      this.id as string,
    );
  }
}
