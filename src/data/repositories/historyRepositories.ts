import { IHistory } from '../../types/IHistory';
import BaseRepository from './baseRepository';

export class HistoryRepositories extends BaseRepository<IHistory> {

  getTable(): string {
    return 'History';
  }

}