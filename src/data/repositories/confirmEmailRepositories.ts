import crypto from 'crypto';
import { AuthenticationError } from 'apollo-server-express';
import { UserRepositories } from './userRepositories';
import { IUser } from '../../types/IUser';
import { transporter } from '../../utils/email';
import config from '../../libs/config';
import { InviteRepositories } from './inviteRepositories';

export class ConfirmEmailRepositories extends UserRepositories {
  needContextUser(): boolean | undefined {
    return undefined;
  }

  public async sendEmailConfirmationLink(email: string) {
    let model: IUser | null;
    if (this.contextUser) {
      model = this.contextUser;
    } else {
      model = (await this.getByFields({ confirmEmail: email }))[0];
    }
    if (!model) {
      throw new AuthenticationError('Почта уже подтверждена');
    }
    const confirmEmailCode = crypto
      .getRandomValues(new Uint32Array(1))[0]
      .toString(16);
    const confirmEmailExpires = Date.now() + 3600000;
    model.confirmEmailCode = confirmEmailCode;
    model.confirmEmailExpires = new Date(confirmEmailExpires);
    model.confirmEmail = email;
    await new ConfirmEmailRepositories(this.contextUser, model.id).edit(model);
    const result = await transporter.sendMail({
      from: `S3APP <${config.emailTransporter?.auth.user}>`,
      to: email,
      subject: 'Ссылка на подтверждение почты',
      text: `Ссылка на подтверждение почты: ${config.host}/confirm-email/?code=${confirmEmailCode}`,
    });
    if (result) {
      return true;
    }
    return false;
  }
    
  public async ConfirmEmail(code: string) {
    let model: IUser;
    if (this.contextUser) {
      model = this.contextUser;
      if (model.confirmEmailCode !== code) {
        throw new AuthenticationError('Временный код не найден');
      }
    } else {
      model = (await this.getByFields({ confirmEmailCode: code }))[0];
      if (!model) {
        throw new AuthenticationError('Временный код не найден');
      }
    }
    if (
      model.confirmEmailExpires &&
                Date.now() - new Date(model.confirmEmailExpires).getTime() > 3600000
    ) {
      throw new AuthenticationError('Токен истек');
    }
    if (!model.confirmEmail) {
      throw new AuthenticationError('Почта не найдена');
    }
    const emailModel = await this.getByEmail(model.confirmEmail);
    if (emailModel) {
      throw new AuthenticationError('Почта уже подтверждена');
    }
    if (!model.email) {
      await new InviteRepositories().acceptInvite(model.confirmEmail, model.id as string);
    }
    model.email = model.confirmEmail;
    model.confirmEmail = '';
    model.confirmEmailExpires = undefined;
    model.confirmEmailCode = '';
    if (model.id) {
      await new ConfirmEmailRepositories(this.contextUser, model.id).edit(model);
    }
    return true;
  }
}