import BaseRepository from './baseRepository';
import { UserRepositories } from './userRepositories';
import { IUserOrganizationInvite } from '../../types/IUserOrganizationInvite';
import { transporter } from '../../utils/email';
import config from '../../libs/config';
import { DomainRepositories } from './domainRepositories';
import { Knex } from 'knex';
import { OrganizationRepositories } from './organizationRepositories';
import { AccessRole } from '../../types/IUserOrganizationLink';
export class InviteRepositories extends BaseRepository<IUserOrganizationInvite> {
  getTable(): string {
    return 'UserOrganizationInvite';
  }

  needContextUser(): boolean | undefined {
    return undefined;
  }

  public async create(newData: Partial<IUserOrganizationInvite>): Promise<IUserOrganizationInvite> {
    newData.inviterUserId = this.contextUser?.id;
    newData.isAccepted = false;
    newData.email = newData.email?.toLowerCase();
    const data = await super.create(newData);
    const domain = await new DomainRepositories(this.contextUser, newData.organizationId as string).getData();
    await transporter.sendMail({
      from: `S3APP <${config.emailTransporter?.auth.user}>`,
      to: newData.email,
      subject: 'Приглашение в S3App',
      text: `Уважаемый ${newData.name}!

Вас пригласили в ${domain.name}.

Зарегистрируйтесь по адресу ${config.host}/register${config.isLicense ? ` с кодом ${newData.license}` : ''}`,
    });
    return data;
  }

  public async checkCreateEditData(newData: Partial<IUserOrganizationInvite>, isCreate?: boolean): Promise<boolean> {
    if (isCreate) {
      const invite = await new InviteRepositories(this.contextUser).getByFields({ email: newData.email?.toLowerCase() });
      if (invite.length > 0) {
        throw new Error('Invite already exists');
      }
    }
    return true;
  }

  public async checkEntityAccess(entity: IUserOrganizationInvite): Promise<boolean> {
    if (this.domainHierarchy && entity.organizationId) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      if (!domainIds.includes(entity.organizationId as string)) {
        throw new Error('Access denied');
      }
    }
    return !!entity;  
  }

  async filterEntities(knex: Knex.QueryBuilder) {
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const domainIds = await domainHierarchy.getUserDomainIds();
      return knex.whereIn('organizationId', domainIds);
    }
    return knex;
  }

  public async checkEntitiesOfOrganization(ids: string[], domainId: string): Promise<IUserOrganizationInvite[]> {
    const entities = await this.getByIds(ids);
    if (this.domainHierarchy) {
      const domainHierarchy = await this.domainHierarchy;
      const organizationId = domainHierarchy.getOrganizationFromDomain(domainId)?.domain.id;
      if (!organizationId) {
        throw new Error('Access denied');
      }
      entities.forEach(entity => {
        if (!domainHierarchy.checkDomainInOrganization(entity.organizationId as string, organizationId)) {
          throw new Error('Access denied');
        }
      });
    }

    return entities;
  }

  public async getByDomainId(domainId: string): Promise<IUserOrganizationInvite[]> {
    const domainHierarchy = await this.domainHierarchy;
    const organizationId = domainHierarchy?.getOrganizationFromDomain(domainId)?.domain.id;
    if (!organizationId) {
      throw new Error('Access denied');
    }
    return this.getByFields({ organizationId });
  }

  public async acceptInvite(email: string, userId: string): Promise<boolean> {
    const invite = (await this.getByFields({ email: email.toLowerCase() }))[0];
    if (!invite) {
      return false;
    }
    const user = await new UserRepositories(undefined, userId).getData();
    if (!user) {
      throw new Error('User not found');
    }
    invite.isAccepted = true;
    invite.acceptedAt = new Date();
    invite.invitedUserId = userId;
    const organizationRepository = new OrganizationRepositories(undefined, invite.organizationId as string);
    organizationRepository.addUsersToOrganization([{ userId, accessRole: AccessRole.user }]);
    await new InviteRepositories(this.contextUser, invite.id).edit(invite);
    return true;
  }


}