import { Knex } from 'knex';
import { IEntity } from '../../types/IEntity';
import { IUser } from '../../types/IUser';
import dbClient from '../../utils/knex';

abstract class BaseInternalRepository<T extends IEntity> {
  id?: string;

  _data?: Promise<T>;

  contextUser?: IUser;

  isInternal = true;

  get data(): Promise<T> | undefined {
    if (!this._data) {
      this._data = this.getData();
    }
    return this._data;
  }

  constructor(
    contextUser: IUser | undefined = undefined,
    id: string | T | undefined = undefined,
    isInternal = false,
  ) {
    this.isInternal = isInternal;
    if (!this.isInternal && this.needContextUser() !== undefined) {
      if (!!contextUser !== this.needContextUser()) {
        throw new Error('Access denied');
      }
    }
    this.contextUser = contextUser;
    if (id && typeof id === 'string') {
      this.id = id;
    } else if (id && typeof id === 'object') {
      this.id = id.id;
      this._data = Promise.resolve(id);
    }
  }

  abstract getTable(): string;

  needContextUser(): boolean | undefined {
    return true;
  }

  async checkEntityAccess(entity: Partial<T>) {
    return !!entity;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async checkCreateEditData(newData: Partial<T>, isCreate: boolean = false) {
    return !!newData;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async checkEntitiesOfOrganization(ids: string[], organizationId: string): Promise<T[]> {
    const entities = await this.getByIds(ids);
    return entities;
  }

  async filterEntities(knex: Knex.QueryBuilder) {
    return knex;
  }

  async getData(): Promise<T> {
    if (this._data) return this._data;
    if (!this.id) throw new Error('Id not found');
    const entity = await dbClient
      .select('*')
      .from(this.getTable())
      .where('id', this.id)
      .andWhere('isDeleted', false)
      .first();
    if (!entity) throw new Error('Entity not found');
    await this.checkEntityAccess(entity);
    return entity;
  }

  orderBy(): string {
    return `${this.getTable()}.createdAt`;
  }

  orderByDirection(): string {
    return 'desc';
  }

  async getAll(): Promise<T[]> {
    const result = await this.filterEntities(
      dbClient
        .select(`${this.getTable()}.*`)
        .from(this.getTable())
        .where(`${this.getTable()}.isDeleted`, false)
        .orderBy(this.orderBy(), this.orderByDirection()),
    );
    return result;
  }

  async getByIds(ids: string[]): Promise<T[]> {
    const result = await this.filterEntities(
      dbClient
        .select(`${this.getTable()}.*`)
        .from(this.getTable())
        .whereIn(`${this.getTable()}.id`, ids)
        .andWhere(`${this.getTable()}.isDeleted`, false)
        .orderBy(this.orderBy(), this.orderByDirection()),
    );
    return result;
  }

  async getByFields(fields: Partial<T>): Promise<T[]> {
    const result = await this.filterEntities(
      dbClient
        .select(`${this.getTable()}.*`)
        .from(this.getTable())
        .where(fields)
        .andWhere(`${this.getTable()}.isDeleted`, false)
        .orderBy(this.orderBy(), this.orderByDirection()),
    );
    return result;
  }

  async getByQuery(
    callback: (knex: Knex.QueryBuilder) => Knex.QueryBuilder,
  ): Promise<T[]> {
    const result = await this.filterEntities(
      callback(
        dbClient
          .select(`${this.getTable()}.*`)
          .from(this.getTable())
          .where(`${this.getTable()}.isDeleted`, false)
          .orderBy(this.orderBy(), this.orderByDirection()),
      ),
    );
    return result;
  }

  async create(newData: Partial<T>): Promise<T> {
    newData.createdAt = new Date();
    await this.checkEntityAccess(newData);
    await this.checkCreateEditData(newData, true);
    const [created] = await dbClient(this.getTable())
      .insert(newData)
      .returning('*');
    this.id = created.id;
    return created;
  }

  async edit(newData: Partial<T>): Promise<T> {
    await this.getData();
    newData.updatedAt = new Date();
    const data = await this.data;
    await this.checkEntityAccess(data as T);
    await this.checkEntityAccess(newData);
    await this.checkCreateEditData(newData);
    const [edited] = await dbClient(this.getTable())
      .where('id', this.id)
      .andWhere('isDeleted', false)
      .update(newData)
      .returning('*');
    return edited;
  }

  async delete(): Promise<boolean> {
    await this.getData();
    await dbClient(this.getTable())
      .where('id', this.id)
      .andWhere('isDeleted', false)
      .update({
        isDeleted: true,
        deletedAt: new Date(),
      });
    return true;
  }
}

export default BaseInternalRepository;
